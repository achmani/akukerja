<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInitialization extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('jobs_regions', function (Blueprint $table) {
            $table->dropForeign(['region_id']);
            $table->foreign('region_id')->references('id')->on('regencies');
        });
        
        Schema::create('educations', function (Blueprint $table) {
            $table->increments('education_id');
            $table->string('education_name');
            $table->boolean('education_active');
            $table->timestamps();
        });

        Schema::create('specializations', function (Blueprint $table) {
            $table->string('specialization_id')->primary();
            $table->string('specialization_name');
            $table->boolean('specialization_active');
            $table->timestamps();
        });

        Schema::create('specializations_sub', function (Blueprint $table) {
            $table->increments('specialization_sub_id');
            $table->string('specialization_id');
            $table->foreign('specialization_id')->references('specialization_id')->on('specializations');
            $table->string('specialization_sub_name');
            $table->boolean('specialization_sub_active');
            $table->timestamps();
        });

        Schema::create('industries', function (Blueprint $table) {
            $table->increments('industries_id');
            $table->string('industries_name');
            $table->boolean('industries_active');
            $table->timestamps();
        });

        Schema::create('users_experience', function (Blueprint $table) {
            $table->bigIncrements('user_experience_id');
            $table->string('user_experience_position');
            $table->unsignedBigInteger('user_experience_company');
            $table->foreign('user_experience_company')->references('company_id')->on('companies');
            $table->string('user_experience_company_non');
            $table->unsignedInteger('user_experience_specialization');
            $table->foreign('user_experience_specialization')->references('specialization_sub_id')->on('specializations_sub');
            $table->unsignedInteger('user_experience_industry');
            $table->foreign('user_experience_industry')->references('industries_id')->on('industries');
            $table->boolean('user_experience_status');
            $table->date('user_experience_start');
            $table->text('user_experience_desc');
            $table->timestamps();
        });

        Schema::create('jobs_industries', function (Blueprint $table) {
            $table->unsignedBigInteger('job_id');
            $table->foreign('job_id')->references('job_id')->on('jobs');
            $table->unsignedInteger('industries_id');
            $table->unsignedInteger('job_industries_seq');
            $table->foreign('industries_id')->references('industries_id')->on('industries');
            $table->primary(['job_id', 'job_industries_seq']);
            $table->timestamps();
        });

        Schema::create('jobs_specializations', function (Blueprint $table) {
            $table->unsignedBigInteger('job_id');
            $table->foreign('job_id')->references('job_id')->on('jobs');
            $table->unsignedInteger('specialization_sub_id');
            $table->unsignedInteger('job_specialization_seq');
            $table->foreign('specialization_sub_id')->references('specialization_sub_id')->on('specializations_sub');
            $table->primary(['job_id', 'job_specialization_seq']);
            $table->timestamps();
        });

        Schema::create('users_education', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('education_id');
            $table->foreign('education_id')->references('education_id')->on('educations');
            $table->unsignedBigInteger('region_id');
            $table->foreign('region_id')->references('id')->on('regencies');
            $table->unsignedInteger('user_education_seq');
            $table->primary(['user_id', 'education_id','user_education_seq']);
            $table->string('user_education_name');
            $table->string('user_education_study');
            $table->text('user_education_desc');
            $table->year('user_education_year');
            $table->integer('user_education_month');
            $table->timestamps();
        });

        Schema::create('users_expertise', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('user_expertise_seq');
            $table->primary(['user_id', 'user_expertise_seq']);
            $table->string('user_expertise_name');
            $table->string('user_expertise_level');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('educations');
    }
}
