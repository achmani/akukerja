<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Initializationdatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('company_id');
            $table->string('company_name');
            $table->string('company_address');
            $table->string('company_mail');
            $table->string('company_phone');
            $table->integer('company_industry');
            $table->integer('company_processing_time');
            $table->string('company_benefits');
            $table->integer('company_min_employee');
            $table->integer('company_max_employee');
            $table->text('company_overview');
            $table->string('company_image');
            $table->string('company_thumbnail');
            $table->integer('company_status')->default(1);
            $table->timestamps();
        });

        Schema::create('regions', function (Blueprint $table) {
            $table->bigIncrements('region_id');
            $table->string('region_name');
            $table->string('region_lat');
            $table->string('region_long');
            $table->integer('region_status')->default(1);
            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('categories_id');
            $table->string('categories_name');
            $table->string('categories_logo');
            $table->integer('categories_status')->default(1);
            $table->timestamps();
        });

        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('job_id');
            $table->string('job_title');
            $table->unsignedBigInteger('company_id');
            $table->foreign('company_id')->references('company_id')->on('companies');
            $table->dateTime('job_posted');
            $table->dateTime('job_expired');
            $table->text('job_description');
            $table->integer('job_status');
            $table->integer('min_salary')->default(0);
            $table->integer('max_salary')->default(0);
            $table->timestamps();
        });

        Schema::create('jobs_regions', function (Blueprint $table) {
            $table->unsignedBigInteger('job_id');
            $table->foreign('job_id')->references('job_id')->on('jobs');
            $table->unsignedBigInteger('region_id');
            $table->foreign('region_id')->references('region_id')->on('regions');
            $table->timestamps();
        });

        Schema::create('jobs_categories', function (Blueprint $table) {
            $table->unsignedBigInteger('job_id');
            $table->foreign('job_id')->references('job_id')->on('jobs');
            $table->unsignedBigInteger('categories_id');
            $table->unsignedInteger('job_categories_seq');
            $table->foreign('categories_id')->references('categories_id')->on('categories');
            $table->primary(['job_id', 'job_categories_seq']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('jobs_regions');
        // Schema::dropIfExists('jobs_categories');
        // Schema::dropIfExists('jobs_description');
        // Schema::dropIfExists('jobs_responsibilities');
        // Schema::dropIfExists('jobs_qualifications');
        // Schema::dropIfExists('companies');
        // Schema::dropIfExists('regions');
        // Schema::dropIfExists('categories');
        // Schema::dropIfExists('jobs');
    }
}
