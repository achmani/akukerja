<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('provinces', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('alt_name');
            $table->string('latitude');
            $table->string('longitude');
        });

        Schema::create('regencies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('province_id');
            $table->foreign('province_id')->references('id')->on('provinces');
            $table->string('name');
            $table->string('alt_name');
            $table->string('latitude');
            $table->string('longitude');
        });

        Schema::create('districts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('regency_id');
            $table->foreign('regency_id')->references('id')->on('regencies');
            $table->string('name');
            $table->string('alt_name');
            $table->string('latitude');
            $table->string('longitude');
        });

        Schema::create('villages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('district_id');
            $table->foreign('district_id')->references('id')->on('districts');
            $table->string('name');
            $table->string('alt_name');
            $table->string('latitude');
            $table->string('longitude');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('villages');
        // Schema::dropIfExists('districts');
        // Schema::dropIfExists('regencies');
        // Schema::dropIfExists('provinces');
    }
}
