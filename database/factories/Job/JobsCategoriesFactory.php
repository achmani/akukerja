<?php

namespace Database\Factories\Job;

use App\Models\Job\JobsCategories;
use Illuminate\Database\Eloquent\Factories\Factory;

class JobsCategoriesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = JobsCategories::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'categories_id' => $this->faker->numberBetween(1,6)
        ];
    }
}
