<?php

namespace Database\Factories\Job;

use App\Models\Job\Jobs;
use App\Models\Companies;
use Faker\Provider\ar_JO\Company;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class JobsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Jobs::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $post_date = $this->faker->date('Y-m-d H:i:s','2021-12-30');
        $expired_date = date('Y-m-d H:i:s',strtotime($post_date . "+10 days"));
        $min_salary = $this->faker->numberBetween(500000,4500000);
        $max_salary = $this->faker->numberBetween($min_salary,10000000);
        
        $description = "";

        //JobsDescription
        $count_temp = rand(0, 10);
        
        if($count_temp >= 1){
            $description .= "<b>Job Description</b><ul>";
        }
        for ($i = 1; $i <= $count_temp; $i++) {
            $description .= '<li>'.$this->faker->text(100)."</li>";
        }
        if($count_temp >= 1){
            $description .= "</ul>";
        }

        //JobsQualifications
        $count_temp = rand(0, 10);
        if($count_temp >= 1){
            $description .= "<b>Job Qualification</b><ul>";
        }
        for ($i = 1; $i <= $count_temp; $i++) {
            $description .= '<li>'.$this->faker->text(100)."</li>";
        }
        if($count_temp >= 1){
            $description .= "</ul>";
        }

        //JobsResponsibilities
        $count_temp = rand(0, 10);
        if($count_temp >= 1){
            $description .= "<b>Job Responsibilities</b><ul>";
        }
        for ($i = 1; $i <= $count_temp; $i++) {
            $description .= '<li>'.$this->faker->text(100)."</li>";
        }
        if($count_temp >= 1){
            $description .= "</ul>";
        }

        $min_id = Companies::select('company_id')->min('company_id');
        $max_id = Companies::max('company_id');

        return [
            'job_title' => $this->faker->jobTitle,
            'company_id' => $this->faker->numberBetween($min_id,$max_id),
            'job_posted' => $post_date,
            'job_expired' => $expired_date, // password
            'job_description' => $description,
            'min_salary' => $min_salary,
            'max_salary' => $max_salary,
            'job_status' => 1,
        ];
    }
}
