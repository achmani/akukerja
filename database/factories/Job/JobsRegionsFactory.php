<?php

namespace Database\Factories;

use App\Models\JobsRegions;
use Illuminate\Database\Eloquent\Factories\Factory;

class JobsRegionsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = JobsRegions::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
