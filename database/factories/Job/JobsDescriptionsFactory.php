<?php

namespace Database\Factories\Job;

use App\Models\Job\JobsDescriptions;
use Illuminate\Database\Eloquent\Factories\Factory;

class JobsDescriptionsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = JobsDescriptions::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'job_descriptions_text' => $this->faker->text(200),
        ];
    }
}
