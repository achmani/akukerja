<?php

namespace Database\Factories\job;

use App\Models\Job\JobsResponsibilities;
use Illuminate\Database\Eloquent\Factories\Factory;

class JobsResponsibilitiesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = JobsResponsibilities::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'job_responsibilities_text' => $this->faker->text(200),
        ];
    }
}
