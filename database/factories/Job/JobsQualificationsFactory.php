<?php

namespace Database\Factories\Job;

use App\Models\Job\JobsQualifications;
use Illuminate\Database\Eloquent\Factories\Factory;

class JobsQualificationsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = JobsQualifications::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'job_qualifications_text' => $this->faker->text(200),
        ];
    }
}
