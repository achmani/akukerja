<?php

namespace Database\Factories;

use App\Models\Companies;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompaniesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Companies::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'company_name' => $this->faker->unique()->company,
            'company_address' => $this->faker->unique()->address,
            'company_mail' => $this->faker->unique()->companyEmail,
            'company_phone' => $this->faker->phoneNumber,
            'company_industry' => $this->faker->numberBetween(0,20),
            'company_processing_time' => $this->faker->numberBetween(10,30), // password
            'company_benefits' => $this->faker->text,
            'company_min_employee' => $this->faker->numberBetween(10,30),
            'company_max_employee' => $this->faker->numberBetween(1000,10000),
            'company_overview' => $this->faker->text,
            'company_thumbnail' => $this->faker->image("public/app/images/companies"),
            'company_image' => $this->faker->image("public/app/images/companies",100,100),
            'company_status' => 1
        ];
    }
}
