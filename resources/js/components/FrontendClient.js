import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import { loadableReady } from '@loadable/component';
import My from './My';

function FrontendClient() {
    return (
        <BrowserRouter>
            <My ssr={false}/>
        </BrowserRouter>
        
    );
}

export default FrontendClient;

if (document.getElementById('body')) {
    loadableReady(() => {
        ReactDOM.hydrate(<FrontendClient />, document.getElementById('body'));
    });
}
