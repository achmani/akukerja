import { fromJSON } from "postcss";
import React, { Component } from "react";
import { Route, Link, Switch } from "react-router-dom";
import loadable from "@loadable/component";
// import Home from "./Home";
// import About from "./About";
const Home = loadable(() => import("./Home"), { ssr: true });
const About = loadable(() => import("./About"), { ssr: true });

// const My = () => (
//     <div>
//         <ul>
//             <li><Link to="/" >Home</Link></li>
//             <li><Link to="/about" >About</Link></li>
//         </ul>

//         <hr />

//         <Switch>
//             <Route path="/about" component={About} />
//             <Route path="/" component={Home} />
//         </Switch>
//     </div>
// )

export class My extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/about">About</Link>
                        <Link to="/asas">{this.props.ssr}</Link>
                    </li>
                </ul>

                <hr />

                <Switch>
                    <Route path="/about" component={About} />
                    <Route path="/" component={Home} />
                </Switch>
            </div>
        );
    }
}

export default My;

// function My() {
//     const Home = loadable(() => import("./Home"));
//     const About = loadable(() => import("./About"));
//     return (
//         <div>
//             <ul>
//                 <li>
//                     <Link to="/">Home</Link>
//                 </li>
//                 <li>
//                     <Link to="/about">About</Link>
//                 </li>
//             </ul>

//             <hr />

//             <Switch>
//                 <Route path="/about" component={About} />
//                 <Route path="/" component={Home} />
//             </Switch>
//         </div>
//     );
// }

// export default My;
