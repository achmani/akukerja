// import React from "react";
// import ReactDOMServer from "react-dom/server";
// import { StaticRouter } from "react-router";

// import My from './My';

// function FrontendServer() {
//     let path = context.path;
//     if(path != '/'){
//         path = '/'+path;
//     }
//     return (
//         <StaticRouter location={path}>
//             <My ssr={true}/>
//         </StaticRouter>
//     );
// }
// const html = ReactDOMServer.renderToStaticMarkup(<FrontendServer />);
// dispatch(html);

// function FrontendClient() {
//     return (
//         <BrowserRouter>
//             <My ssr={false} />
//         </BrowserRouter>
//     );
// }

// export default FrontendClient;

// if (document.getElementById("body")) {
//     loadableReady(() => {
//         ReactDOM.render(<FrontendClient />, document.getElementById("body"));
//     });
// }


import { renderToString } from "react-dom/server";
import { ChunkExtractor, ChunkExtractorManager } from "@loadable/server";
import My from "./My";
const statsFile = '../loadable-stats.json';
const extractor = new ChunkExtractor({ statsFile });
const html = renderToString(
    <ChunkExtractorManager extractor={extractor}>
        <BrowserRouter>
            <My ssr={false} />
        </BrowserRouter>
    </ChunkExtractorManager>
);

console.log(html);

const scriptTags = extractor.getScriptTags() // or extractor.getScriptElements();
