<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body class="bg-white">
    <div id="body" class="container-fluid p-0">
        {{-- {!! ssr('js/app-server.js')->context('path', Request::path())->render() !!} --}}
    </div>
</body>
    <script src="{{ asset('js/app-client.js') }}"></script>
    <script src="{{ asset('js/app-server.js') }}"></script>
</html>