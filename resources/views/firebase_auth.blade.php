<html>

<head>
    <title></title>
</head>

<body>

</body>
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>

<!-- Insert these scripts at the bottom of the HTML, but before you use any Firebase services -->

<!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js"></script>

<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
<script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-analytics.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-firestore.js"></script>

<script>
    $(document).ready(function() {

        // Your web app's Firebase configuration
        // For Firebase JS SDK v7.20.0 and later, measurementId is optional
        var firebaseConfig = {
            apiKey: "AIzaSyBChNUvrZUow50BkT5S8Wm2005IKLO70jA",
            authDomain: "akukerja-32620.firebaseapp.com",
            databaseURL: "https://akukerja-32620.firebaseio.com",
            projectId: "akukerja-32620",
            storageBucket: "akukerja-32620.appspot.com",
            messagingSenderId: "774964727793",
            appId: "1:774964727793:web:342f25ce9617e264d2fe50",
            measurementId: "G-N8B128X29C"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
        firebase.analytics();

        var provider = new firebase.auth.GoogleAuthProvider();

        firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            // console.log(user.additionalUserInfo);
            console.log(result.credential);
            console.log("|||||||||||||||||||||||||||||||||||||||||");
            $.ajax({
                url: "{{ route('login_firebase') }}",
                method: "POST", //First change type to method here
                data: {
                    Firebasetoken: result.credential.idToken, // Second add quotes on the value.
                },
                success: function(response) {
                    console.log(response);
                },
                error: function() {
                    console.log("//////////////////////////////////////////////////");
                }
            });

        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            console.log(error);
            // ...
        });

        firebase.auth().onAuthStateChanged(user => {

            user.getIdToken().then(function(accessToken) {
                // I personally store this token using Vuex 
                // so i can watch it and detect its change to act accordingly.
                console.log("----------------------");
                // console.log(accessToken);
                $.ajax({
                    url: "{{ route('login_firebase') }}",
                    method: "POST", //First change type to method here

                    data: {
                        Firebasetoken: accessToken, // Second add quotes on the value.
                    },
                    success: function(response) {
                        console.log(response);
                    },
                    error: function() {
                        console.log("error2");
                    }
                });
            })

        });

    });

</script>

</html>
