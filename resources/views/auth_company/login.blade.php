@extends('layouts.auth')
@section('title')
Login
@endsection
@section('content')
    <div class="mt-12">
        <form method="POST" action="{{ route('login.auth') }}">
            @csrf
            <div>
                <div class="text-sm font-bold text-gray-700 tracking-wide">Email Address</div>
                <input class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
                    name="email" type="" placeholder="mike@gmail.com">
            </div>
            <div>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="mt-8">
                <div class="flex justify-between items-center">
                    <div class="text-sm font-bold text-gray-700 tracking-wide">
                        Password
                    </div>
                    <div>
                        <a class="text-xs font-display font-semibold text-indigo-600 hover:text-indigo-800
                            cursor-pointer">
                            Forgot Password?
                        </a>
                    </div>
                </div>
                <input class="w-full text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
                    type="password" placeholder="Enter your password" name="password">
            </div>
            <div>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="mt-10">
                <button class="bg-blue-500 text-gray-100 p-4 w-full rounded-full tracking-wide
                    font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-indigo-600
                    shadow-lg">
                    Login
                </button>
            </div>
        </form>
    </div>
@endsection
@section('sideimage')
@endsection
