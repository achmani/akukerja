<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('app/assets/library/tailwind/tailwind.min.css') }}" rel="stylesheet">
    <link href="{{ asset('app/assets/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('app/assets/library/slick/slick.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('app/assets/library/slick/slick-theme.css') }}" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link href="{{ asset('app/assets/css/select2.custom.css') }}" rel="stylesheet" />
    <link rel="shortcut icon" type="image/ico" href="{{ asset('app/images/defaults/akukerjaicon.ico') }}" />
    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}
    {!! JsonLd::generate() !!}
</head>

<body id="body" class="w-full overflow-x-hidden">
    <div class="w-full relative">
        <div class="w-full h-16 bg-white shadow-lg flex px-5">
            <a href="{{ route('index') }}">
                <img class="h-10 mt-3" src="{{ asset('app/images/defaults/akukerja.png') }}"></img>
            </a>
        </div>
    </div>
    @yield('content')
    <footer class="footer bg-white relative pt-1 border-b-2 border-blue-700">
        <div class="container mx-auto px-6">

            <div class="sm:flex sm:mt-8">
                <div class="mt-8 sm:mt-0 sm:w-full sm:px-8 flex flex-col md:flex-row justify-between">
                    <!-- <div class="flex flex-col">
                        <span class="font-bold text-gray-700 uppercase mb-2">Footer header 1</span>
                        <span class="my-2"><a href="#" class="text-blue-700  text-md hover:text-blue-500">link 1</a></span>
                        <span class="my-2"><a href="#" class="text-blue-700  text-md hover:text-blue-500">link 1</a></span>
                        <span class="my-2"><a href="#" class="text-blue-700  text-md hover:text-blue-500">link 1</a></span>
                    </div>
                    <div class="flex flex-col">
                        <span class="font-bold text-gray-700 uppercase mt-4 md:mt-0 mb-2">Footer header 2</span>
                        <span class="my-2"><a href="#" class="text-blue-700 text-md hover:text-blue-500">link 1</a></span>
                        <span class="my-2"><a href="#" class="text-blue-700  text-md hover:text-blue-500">link 1</a></span>
                        <span class="my-2"><a href="#" class="text-blue-700 text-md hover:text-blue-500">link 1</a></span>
                    </div>
                    <div class="flex flex-col">
                        <span class="font-bold text-gray-700 uppercase mt-4 md:mt-0 mb-2">Footer header 3</span>
                        <span class="my-2"><a href="#" class="text-blue-700  text-md hover:text-blue-500">link 1</a></span>
                        <span class="my-2"><a href="#" class="text-blue-700  text-md hover:text-blue-500">link 1</a></span>
                        <span class="my-2"><a href="#" class="text-blue-700  text-md hover:text-blue-500">link 1</a></span>
                    </div> -->
                </div>
            </div>
        </div>
        <div class="container mx-auto px-6">
            <div class="mt-16 border-t-2 border-gray-300 flex flex-col items-center">
                <div class="sm:w-2/3 text-center py-6">
                    <p class="text-sm text-blue-700 font-bold mb-2">
                        © 2021 AkuKerja
                    </p>
                </div>
            </div>
        </div>
    </footer>
</body>
<script src="{{ asset('app/assets/library/jquery/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/assets/library/slick/slick.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{ asset('app/assets/js/issue-select2.js') }}"></script>
@include ('layouts.js')
@yield('scripts')

</html>