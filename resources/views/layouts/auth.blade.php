<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Contact Form Template</title>
    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}
    {!! JsonLd::generate() !!}
    <link href="{{ asset('app/assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('app/assets/library/tailwind/tailwind.min.css') }}" rel="stylesheet">
</head>

<body class>
    <div class="lg:flex">
        <div class="lg:w-1/2 xl:max-w-screen-sm">
            <div class="py-12 bg-blue-100 lg:bg-white flex justify-center lg:justify-start lg:px-12">
                <img class="h-14 mt-3" src="{{ asset('app/images/defaults/akukerja.png') }}"></img>
            </div>
            <div class="mt-10 px-12 sm:px-24 md:px-48 lg:px-12 xl:px-24 xl:max-w-2xl">
                <h2 class="text-center text-4xl text-blue-900 font-display font-semibold lg:text-left xl:text-5xl
                    xl:text-bold">@yield('title')</h2>
                @yield('content')
            </div>
        </div>
        <div class="hidden lg:flex items-center justify-center bg-blue-100 flex-1 h-screen">
            <div class="max-w-xs transform duration-200 hover:scale-110 cursor-pointer">
                @yield('sideimage')
            </div>
        </div>
    </div>
</body>

</html>