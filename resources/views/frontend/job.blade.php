@extends('layouts.home')
@section('content')
    <div class="lg:container mx-auto mt-5 flex flex-wrap">
        <div class="w-full lg:w-8/12 lg:mx-0 p-5 border-2 rounded-md shadow-md mb-4 mx-5">
            <div class="w-full">
                <img class="w-28 h-16" src="{{ asset(str_replace('public/', '', $job->companies->company_thumbnail)) }}"
                    alt="{{ $job->job_title }} - {{ $job->companies->company_name }}">
                <div class="mt-3 font-bold">
                    {{ $job->job_title }}
                </div>
                <div class="my-3 font-medium text-blue-500">
                    {{ $job->companies->company_name }}
                </div>
                <div class="mt-1">
                    {{ implode(', ', $job->regions->pluck('name')->toArray()) }}
                </div>
                <div class="mt-1">
                    IDR {{ number_format($job->min_salary, 2, ',', '.') }} -
                    {{ number_format($job->max_salary, 2, ',', '.') }}
                </div>
                <div class="mt-3 text-gray-400">
                    {{ implode(', ', $job->categories->pluck('categories_name')->toArray()) }}
                </div>
                <div class="mt-1 text-gray-400">
                    diposting {{ $job->job_posted->diffForHumans() }}
                </div>
                <div class="flex my-5">
                    <a href="https://akukerja.page.link?amv=0&apn=akukerja.com&ibi=akukerja.com&imv=0&link=https%3A%2F%2Fakukerja%3Fjobid%3D{{ $job->job_id }}"
                        class="mr-3 cursor-pointer py-2 px-5 bg-blue-500 font-medium rounded-md text-white">Apply</a>
                    {{-- <div
                        class="mx-3 cursor-pointer py-2 px-5 font-medium rounded-md text-blue-500 border-blue-500 border-2">
                        Bookmark</div> --}}
                    <div
                        class="mx-3 cursor-pointer py-2 px-5 font-medium rounded-md text-blue-500 border-blue-500 border-2">
                        Share</div>
                </div>
                <hr class="my-3 border-t-2">
            </div>
            <div class="w-full">
                {!! $job->job_description !!}
                <div class="mb-2 mt-4 font-bold">About the company</div>
                {{-- <img class="w-28 h-16" src="tokopedia.jpg" alt=""> --}}
                <div class="my-2">
                    {!! $job->companies->company_overview !!}
                </div>
            </div>
        </div>
        <div class="w-full lg:w-4/12 px-5">
            <div class="mb-2 font-bold">Similiar jobs</div>
            <div class="w-full flex flex-col">
                @foreach ($similar_jobs as $item)
                    <a href="{{ route('job', $item->job_id) }}" class="card py-5 border-2 rounded-lg mb-4 w-full">
                        <div class="w-20 h-12 mx-5">
                            <img class="h-10"
                                src="{{ asset(str_replace('public/', '', $item->companies->company_thumbnail)) }}"
                                alt="{{ $item->job_title }} - {{ $item->companies->company_name }}"
                                class="object-contain">
                        </div>
                        <div class="p-2 px-5 bg-blue-500 max-w-max font-bold text-white">
                            {{ $item->job_title }}
                        </div>
                        <div class="mx-5 mt-3 font-serif">
                            {{ $item->companies->company_name }}
                        </div>
                        <div class="mx-5 mt-1 font-serif text-sm">
                            {{ $item->regions }}
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
    </script>
@endsection
