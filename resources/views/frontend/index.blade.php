@extends('layouts.home')
@section('content')
<div class="w-full relative z-10">
    <div class="relative w-full pt-sm-35 pt-50 bg-gray-800 bg-cover bg-no-repeat bg-center" style="background-image:url( {{ asset('app/assets/images/pexels-fauxels-3183150.jpg') }} )">
        @include('layouts.search')
    </div>
</div>
<div class="lg:container mx-auto mt-24 px-5">
    <h1 class="text-center font-bold text-3xl md:text-5xl mb-20 mt-40">Paling Banyak Dicari</h1>
    <div class="w-full center">
        @foreach ($job_favorite as $item)
        <a class="outline-none" href="{{ route('job', $item->job_id) }}">
            <div class="card shadow-lg py-5 mx-3 border-2 rounded-lg mb-4">
                <div class="w-20 h-12 mx-5">
                    <img class="h-10" src="{{ asset(str_replace('public/', '', $item->companies->company_thumbnail)) }}" alt="{{ $item->job_title }} - {{ $item->companies->company_name }}" class="object-contain">
                </div>
                <div class="p-2 px-5 bg-primary max-w-max font-bold text-white">
                    {{ $item->job_title }}
                </div>
                <div class="mx-5 mt-3 font-serif">
                    {{ $item->companies->company_name }}
                </div>
                <div class="mx-5 mt-1 font-serif text-sm">
                    {{ $item->regions }}
                </div>
            </div>
        </a>
        @endforeach
    </div>
</div>
<div class="lg:container mx-auto mt-5 px-5">
    <h1 class="text-center font-bold text-3xl md:text-4xl mb-20 mt-20">Bidang di AkuKerja</h1>
    <div class="w-full grid grid-cols-2 md:grid-cols-2 lg:grid-cols-3 gap-2 mb-5">
        @foreach ($categories as $item)
        <div class="desaturate col-span-1 flex flex-col justify-center mb-20">
            <div class="bg-contain bg-no-repeat bg-center p-12" style="background-image: url({{ asset('app/images/categories/' . $item->categories_logo) }})">
            </div>
            <div class="text-center text-2xl font-bold mt-8">{{ $item->categories_name }}</div>
        </div>
        @endforeach
    </div>
</div>
<div class="lg:container mx-auto mt-5 px-5">
    <div class="w-full grid grid-cols-12 gap-2">
        <div class="col-span-12 lg:col-span-5 px-3 lg:px-0">
            <div class="font-bold text-3xl md:text-4xl">
                Apa kata mereka ?
            </div>
            <div class="my-5">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem aspernatur ratione illum
                possimus sequi quo expedita recusandae labore! Eligendi, ut.
            </div>
        </div>
        <div class="col-span-12 lg:col-span-7 center2">
            <div class="outline-none cursor-pointer card shadow-lg py-5 mx-2 border-2 rounded-lg mb-4">
                <div class="w-20 h-12 mx-5">
                    <img src="tokopedia.jpg" alt="" class="object-contain">
                </div>
                <div class="p-2 px-5 bg-primary max-w-max font-bold text-white">
                    Backend Engineer - NodeJS
                </div>
                <div class="mx-5 mt-3 font-serif">
                    PT Tokopedia Indonesia
                </div>
                <div class="mx-5 mt-1 font-serif text-sm">
                    Jakarta Pusat
                </div>
            </div>
            <div class="outline-none cursor-pointer card shadow-lg py-5 mx-2 border-2 rounded-lg mb-4">
                <div class="w-20 h-12 mx-5">
                    <img src="tokopedia.jpg" alt="" class="object-contain">
                </div>
                <div class="p-2 px-5 bg-primary max-w-max font-bold text-white">
                    Backend Engineer - NodeJS
                </div>
                <div class="mx-5 mt-3 font-serif">
                    PT Tokopedia Indonesia
                </div>
                <div class="mx-5 mt-1 font-serif text-sm">
                    Jakarta Pusat
                </div>
            </div>
            <div class="outline-none cursor-pointer card shadow-lg py-5 mx-2 border-2 rounded-lg mb-4">
                <div class="w-20 h-12 mx-5">
                    <img src="tokopedia.jpg" alt="" class="object-contain">
                </div>
                <div class="p-2 px-5 bg-primary max-w-max font-bold text-white">
                    Backend Engineer - NodeJS
                </div>
                <div class="mx-5 mt-3 font-serif">
                    PT Tokopedia Indonesia
                </div>
                <div class="mx-5 mt-1 font-serif text-sm">
                    Jakarta Pusat
                </div>
            </div>
        </div>
    </div>
</div>
<div class="w-full mx-auto mt-5 px-5 md:h-72 bg-gray1">
    <div class="lg:container mx-auto flex flex-col md:flex-row md:justify-end relative md:h-72">
        <div class="relative mt-10 md:mt-0 md:absolute bottom-0 md:-bottom-12 h-72 w-full md:w-1/2 mx-auto md:left-0 bg-contain bg-center md:bg-left-bottom bg-no-repeat z-10" style="background-image: url({{ asset('app/assets/images/laptop-min.png') }});">
        </div>
        <div class="w-full md:w-1/2 px-10" id="1">
            <div class="text-3xl font-bold mt-10">
                Perusahaan
            </div>
            <div class="mt-10">
                Menggunakan antarmuka berbasis web dengan tampilan responsive yang memudahkan akses dari berbagai perangkat. Dihubungkan dengan berbagai pencari kerja dari seluruh indonesia
            </div>
            <div class="w-full grid md:grid-cols-2 grid-cols-1 gap-2 mb-5">
                <button class="p-2 text-center mt-4 bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white border border-blue-500 hover:border-transparent rounded">
                    Daftar Sekarang
                </button>
                <p class="mt-4 p-2 text-blue-700"> Sudah punya akun ? Login disini </p>
            </div>
        </div>
    </div>
</div>
<div class="w-full mx-auto px-5 md:h-72 bg-gray2">
    <div class="lg:container mx-auto flex flex-col-reverse md:flex-row md:justify-start relative md:h-72">
        <div class="w-full md:w-1/2 px-10" id="2">
            <div class="text-3xl font-bold mt-10 md:mt-16">
                Pencari Kerja
            </div>
            <div class="mt-10">
                Bagi kalian para pencari kerja, silahkan segera downlod aplikasi untuk memudakan kalian terhubung dengan perusahaan.
            </div>
            <img class="mb-8 h-14 mt-4" src="{{ asset('app/assets/images/google_play.png') }}" alt="Get it on google play">
        </div>
        <div class="relative mt-10 md:mt-0 md:absolute top-0 md:-top-12 h-72 w-full md:w-1/2 bg-contain md:right-0 bg-center md:bg-right-bottom bg-no-repeat mx-auto" style="background-image: url({{ asset('app/assets/images/phone-min.png') }});">
        </div>
    </div>
</div>
@include('layouts.search_mobile')
@endsection
@section('scripts')
<script>
    let routeRegion = "{{ route('select_region') }}";
</script>
<script src="{{ asset('app/assets/js/index.js') }}"></script>
<script src="{{ asset('app/assets/js/search.js') }}"></script>
@endsection