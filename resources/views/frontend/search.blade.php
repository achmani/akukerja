@extends('layouts.home')
@section('content')
<div class="w-full relative z-10 mt-12">
    @include('layouts.search')
</div>
<div class="lg:container mx-auto mt-28">
    <div class="m-3 font-medium">
        Lowongan Terbaru
    </div>
    <div class="w-100 flex overflow-x-scroll">
        <a href="lowongan.html" class="card flex-shrink-0 shadow-lg py-5 mx-3 border-2 rounded-lg mb-4 w-96">
            <div class="w-20 h-12 mx-5">
                <img src="tokopedia.jpg" alt="" class="object-contain">
            </div>
            <div class="p-2 px-5 bg-green-500 max-w-max font-bold text-white">
                Backend Engineer - NodeJS
            </div>
            <div class="mx-5 mt-3 font-serif">
                PT Tokopedia Indonesia
            </div>
            <div class="mx-5 mt-1 font-serif text-sm">
                Jakarta Pusat
            </div>
        </a>
        <a href="lowongan.html" class="card flex-shrink-0 shadow-lg py-5 mx-3 border-2 rounded-lg mb-4 w-96">
            <div class="w-20 h-12 mx-5">
                <img src="tokopedia.jpg" alt="" class="object-contain">
            </div>
            <div class="p-2 px-5 bg-green-500 max-w-max font-bold text-white">
                Backend Engineer - NodeJS
            </div>
            <div class="mx-5 mt-3 font-serif">
                PT Tokopedia Indonesia
            </div>
            <div class="mx-5 mt-1 font-serif text-sm">
                Jakarta Pusat
            </div>
        </a>
        <a href="lowongan.html" class="card flex-shrink-0 shadow-lg py-5 mx-3 border-2 rounded-lg mb-4 w-96">
            <div class="w-20 h-12 mx-5">
                <img src="tokopedia.jpg" alt="" class="object-contain">
            </div>
            <div class="p-2 px-5 bg-green-500 max-w-max font-bold text-white">
                Backend Engineer - NodeJS
            </div>
            <div class="mx-5 mt-3 font-serif">
                PT Tokopedia Indonesia
            </div>
            <div class="mx-5 mt-1 font-serif text-sm">
                Jakarta Pusat
            </div>
        </a>
        <a href="lowongan.html" class="card flex-shrink-0 shadow-lg py-5 mx-3 border-2 rounded-lg mb-4 w-96">
            <div class="w-20 h-12 mx-5">
                <img src="tokopedia.jpg" alt="" class="object-contain">
            </div>
            <div class="p-2 px-5 bg-green-500 max-w-max font-bold text-white">
                Backend Engineer - NodeJS
            </div>
            <div class="mx-5 mt-3 font-serif">
                PT Tokopedia Indonesia
            </div>
            <div class="mx-5 mt-1 font-serif text-sm">
                Jakarta Pusat
            </div>
        </a>
        <a href="lowongan.html" class="card flex-shrink-0 shadow-lg py-5 mx-3 border-2 rounded-lg mb-4 w-96">
            <div class="w-20 h-12 mx-5">
                <img src="tokopedia.jpg" alt="" class="object-contain">
            </div>
            <div class="p-2 px-5 bg-green-500 max-w-max font-bold text-white">
                Backend Engineer - NodeJS
            </div>
            <div class="mx-5 mt-3 font-serif">
                PT Tokopedia Indonesia
            </div>
            <div class="mx-5 mt-1 font-serif text-sm">
                Jakarta Pusat
            </div>
        </a>
    </div>
</div>
<div class="lg:container mx-auto mt-4 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3">
    <div class="m-3 col-span-1 md:col-span-2 lg:col-span-3 font-medium">
        Menampilkan lowongan ke {{ $jobsearch->firstItem() }} hingga {{ $jobsearch->lastItem() }} dari
        {{ $jobsearch->total() }} lowongan
    </div>
    @foreach ($jobsearch as $item)
    <a href="{{ route('job', $item->job_id) }}">
        <div class="card col-span-1 shadow-lg py-5 mx-3 border-2 rounded-lg mb-4">
            <div class="w-20 h-12 mx-5">
                <img class="h-10" src="{{ asset(str_replace('public/', '', $item->companies->company_thumbnail)) }}" alt="{{ $item->job_title }} - {{ $item->companies->company_name }}" class="object-contain">
            </div>
            <div class="p-2 px-5 bg-primary max-w-max font-bold text-white">
                {{ $item->job_title }}
            </div>
            <div class="mx-5 mt-3 font-serif">
                {{ $item->companies->company_name }}
            </div>
            <div class="mx-5 mt-1 font-serif text-sm">
                {{ $item->regions }}
            </div>
        </div>
    </a>
    @endforeach
</div>
<div class="lg:container mx-auto flex justify-center my-10">
    {!! $jobsearch->appends(\Request::only('regions', 'categories', 'keyword'))->onEachSide(2)->links() !!}
</div>
@include('layouts.search_mobile')
@endsection
@section('scripts')
<script>
    let routeRegion = "{{ route('select_region') }}";
</script>
<script src="{{ asset('app/assets/js/search.js') }}"></script>
<script src="{{ asset('app/assets/js/search_value.js') }}"></script>
@endsection