@extends('layouts.backend')
@section('content')
@csrf
<div class="w-11/12 mx-auto px-5">
    <div class="w-full rounded-sm bg-white shadow p-5 my-5 text-xl text-green-500 font-semibold">
        {{ $job->job_title }}
    </div>
    <div class="w-full flex flex-wrap items-start">
        <div class="w-full grid grid-cols-1 md:grid-cols-2 gap-4">
            @foreach($user_apply as $item)
            @if($loop->index % 2 == 0)
            @include('backend.applicant-card-detail')
            @endif
            <div data-index="{{ $loop->index }}" class="bg-white applican-card col-span-1 rounded-sm shadow p-5 hidden md:block">
                <div class="text-md text-green-500 font-semibold mb-2">{{ $item->users->name." ".$item->users->back_name }} <span class="text-md text-black-500 font-light mb-2">( {{ ($item->users->gender == 1) ? "Pria" : "Wanita" }} )</span></div>
                <ul class="list-disc mx-5">
                    @foreach($item->experience->load(['company']) as $detail)
                    <li class="text-sm my-2">
                        <div>
                            {{ $detail->user_experience_title }}
                        </div>
                        <div>
                            {{ $detail->company->company_experience_name }}
                        </div>
                    </li>
                    @endforeach
                </ul>
                <div class="text-sm my-2"><span>IDR </span>{{ $item->users->salary }}</div>
                <div id="status-{{$item->users->id}}" class="w-full flex mt-4 items-center">
                    <a href="#" data-userid="{{ $item->users->id }}" data-jobid="{{ $item->job_id }}" class="selected-action text-sm text-blue-500 mx-2 ml-0">TERPILIH</a>
                    <a href="#" data-userid="{{ $item->users->id }}" data-jobid="{{ $item->job_id }}" class="interviewed-action text-sm text-blue-500 mx-2">WAWANCARA</a>
                    <a href="#" data-userid="{{ $item->users->id }}" data-jobid="{{ $item->job_id }}" class="notsuitable-action text-sm text-blue-500 mx-2">TIDAK SESUAI</a>
                </div>
            </div>
            @if($loop->index % 2 != 0)
            @include('backend.applicant-card-detail')
            @endif
            @endforeach
            <div class="lg:container mx-auto flex justify-center my-10">
                {!! $user_apply->onEachSide(2)->links() !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>

    $(document).ready(function() {

        $(".interviewed-action").click(function() {

            let user_id = $(this).data("userid");
            let job_id = $(this).data("jobid");
            let _token = $("input[name='_token']").val();

            $.ajax({
                url: "{{ route('company.applicant.update') }}",
                type: 'POST',
                data: {
                    _token: _token,
                    user_id: user_id,
                    job_id: job_id,
                    apply_status: 3
                },
                success: function(data) {

                    $("#status-" + user_id).html("<span class='text-md text-green-500 font-semibold mb-2'>Berhasil Terpilih Untuk Wawancara</span>");

                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'success',
                        title: 'Proses Berhasil'
                    })
                }
            });

        });

        $(".notsuitable-action").click(function() {

            let user_id = $(this).data("userid");
            let job_id = $(this).data("jobid");
            let _token = $("input[name='_token']").val();

            $.ajax({
                url: "{{ route('company.applicant.update') }}",
                type: 'POST',
                data: {
                    _token: _token,
                    user_id: user_id,
                    job_id: job_id,
                    apply_status: 4
                },
                success: function(data) {

                    $("#status-" + user_id).html("<span class='text-md text-green-500 font-semibold mb-2'>Pelamar tidak sesuai</span>");

                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'success',
                        title: 'Proses Berhasil'
                    })
                }
            });

        });


        $(".selected-action").click(function() {

            let user_id = $(this).data("userid");
            let job_id = $(this).data("jobid");
            let _token = $("input[name='_token']").val();

            $.ajax({
                url: "{{ route('company.applicant.update') }}",
                type: 'POST',
                data: {
                    _token: _token,
                    user_id: user_id,
                    job_id: job_id,
                    apply_status: 2
                },
                success: function(data) {
                    
                    $("#status-" + user_id).html("<span class='text-md text-green-500 font-semibold mb-2'>Berhasil Terpilih Untuk Mengikuti Proses Seleksi</span>");

                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'success',
                        title: 'Proses Berhasil'
                    })
                }
            });

        });


    });

    $('.applican-card').on('click', function() {
        let data = $(this).data('index');
        $('.applican-card-detail').addClass('md:hidden');
        if ($(window).width() > 768) {
            if (data % 2 == 0) {
                $('.applican-card-detail-' + data).removeClass('md:hidden');
            } else {
                $('.applican-card-detail-' + data).removeClass('md:hidden');
            }
        } else {
            $('.applican-card-detail-' + data).removeClass('md:hidden');
        }

    });
</script>
@endsection