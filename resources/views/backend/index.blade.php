@extends('layouts.backend')
@section('content')
<div class="w-full mt-4 px-3">
    @if(session()->has('message'))
    <div class="flex justify-center items-center m-1 font-medium py-1 px-2 bg-white rounded-md text-green-700 bg-green-100 border border-green-300 ">
        <div slot="avatar">
            <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle w-5 h-5 mx-2">
                <path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path>
                <polyline points="22 4 12 14.01 9 11.01"></polyline>
            </svg>
        </div>
        <div class="text-xl font-normal  max-w-full flex-initial">
            {{ session()->get('message') }}
        </div>
        <div class="flex flex-auto flex-row-reverse">
            <div>
                <!-- <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x cursor-pointer hover:text-green-400 rounded-full w-5 h-5 ml-2">
                            <line x1="18" y1="6" x2="6" y2="18"></line>
                            <line x1="6" y1="6" x2="18" y2="18"></line>
                        </svg> -->
            </div>
        </div>
    </div>
    @endif
    @foreach ($job as $item)
    <div class="w-full flex justify-between p-5 rounded-sm shadow-md my-5 bg-white">
        <div class="md:w:5/12 lg:w-1/2 w-full">
            <div class="text-xl text-green-500 font-semibold">{{ $item->job_title }}</div>
            <div class="mt-2 text-sm text-gray-400">{{ ( $now > $item->job_posted->addDays(20)->format("Y-m-d") ) ? "Sudah Berakhir" : "Berakhir Pada ".$item->job_posted->addDays(20)->format("Y-m-d") }}</div>
            <div class="flex mt-2">
                <!-- <a href="{{ route('company.applicant',$item->job_id) }}" class="text-sm text-blue-500 mx-3 ml-0">lihat</a> -->
                <a href="{{ route('company.vacancies.form.update',$item->job_id ) }}" class="text-sm text-blue-500">ubah</a>
                <a href="#" class="text-sm text-blue-500 mx-3">hapus</a>
            </div>
            <div class="w-full flex mt-2 md:hidden">
                <div class="w-1/2 rounded-sm shadow-sm border-gray-100 m-1 p-1 text-center">
                    <a href="{{ route('company.applicant',$item->job_id) }}" class="text-lg text-center text-blue-500">{{ $item->applied->where("apply_status",0)->count() }}</br> <span class="text-sm">menunggu</span></a>
                </div>
                <div class="w-1/2 rounded-sm shadow-sm border-gray-100 m-1 p-1 text-center">
                    <a href="{{ route('company.applicant.selected',$item->job_id) }}" class="text-lg text-center text-blue-500">{{ $item->applied->where("apply_status",2)->count() }}</br> <span class="text-sm">terpilih</span></a>
                </div>
            </div>
            <div class="w-full flex mt-2 md:hidden">
                <div class="w-1/2 rounded-sm shadow-sm border-gray-100 m-1 p-1 text-center">
                    <a href="{{ route('company.applicant.interviewed',$item->job_id) }}" class="text-lg text-center text-blue-500">{{ $item->applied->where("apply_status",3)->count() }}</br> <span class="text-sm">wawancara</span></a>
                </div>
                <div class="w-1/2 rounded-sm shadow-sm border-gray-100 m-1 p-1 text-center">
                    <a href="{{ route('company.applicant.notsuitable',$item->job_id) }}" class="text-lg text-center text-blue-500">{{ $item->applied->where("apply_status",3)->count() }}</br> <span class="text-sm">tidak sesuai</span></a>
                </div>
            </div>
        </div>
        <div class="hidden md:block md:w:7/12 lg:w-1/2">
            <div class="flex justify-end">
                <a href="{{ route('company.applicant',$item->job_id) }}" class="flex flex-col w-1/4 items-center px-3 py-1 rounded-sm shadow mx-3">
                    <div class="text-5xl text-blue-500">{{ $item->applied->where("apply_status",0)->count() }}</div>
                    <div class="mt-2 text-center text-blue-500">Belum diproses</div>
                </a>
                <a href="{{ route('company.applicant.selected',$item->job_id) }}" class="flex flex-col w-1/4 items-center px-3 py-1 rounded-sm shadow mx-3">
                    <div class="text-5xl text-blue-500">{{ $item->applied->where("apply_status",2)->count() }}</div>
                    <div class="mt-2 text-center text-blue-500">Terpilih</div>
                </a>
                <a href="{{ route('company.applicant.interviewed',$item->job_id) }}" class="flex flex-col w-1/4 items-center px-3 py-1 rounded-sm shadow mx-3">
                    <div class="text-5xl text-blue-500">{{ $item->applied->where("apply_status",3)->count() }}</div>
                    <div class="mt-2 text-center text-blue-500">Wawancara</div>
                </a>
                <a href="{{ route('company.applicant.notsuitable',$item->job_id) }}" class="flex flex-col w-1/4 items-center px-3 py-1 rounded-sm shadow mx-3">
                    <div class="text-5xl text-blue-500">{{ $item->applied->where("apply_status",4)->count() }}</div>
                    <div class="mt-2 text-center text-blue-500">Tidak Sesuai</div>
                </a>
            </div>
        </div>
    </div>
    @endforeach
    <div class="lg:container mx-auto flex justify-center my-10">
        {!! $job->onEachSide(2)->links() !!}
    </div>
</div>
@endsection
@section('scripts')
@endsection