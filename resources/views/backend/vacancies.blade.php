@extends('layouts.backend')
@section('content')
<!-- <div class="flex justify-center h-14 w-full -bottom-7"> -->
    <div class="w-11/12 md:w-3/5 mb-5 flex flex-wrap justify-center p-1 rounded-md bg-whiteborder-b border-l border-grey-light lg:border-l-0 lg:border-t lg:border-grey-light bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal col-span-full shadow rounded-sm">
        <form method="POST" action="{{ 
            $form ? route('company.vacancies.create') : route('company.vacancies.update') }}" class="w-full">
            @csrf
            <div class="flex flex-wrap -mx-3 mb-6 mt-5">
                @if($job)
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="job_id" value="{{ $job->job_id }}">
                @endif
                <div class="w-full px-3 mb-6">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-job-title">
                        Judul
                    </label>
                    <input class="appearance-none block w-full text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="job-title" name="job_title" type="text" placeholder="IT Surveyor" value="{{ $job ? $job->job_title : '' }}">
                </div>
                <div class="w-full px-3 mb-6">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-job-title">
                        Deskripsi
                    </label>
                    <textarea id="test" name="job_description">
                    {{ $job ? $job->job_description : '' }}
                    </textarea>
                </div>
                <div class="w-full md:w-1/2 mb-6 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-min-salary">
                        Gaji Minimal
                    </label>
                    <input class="appearance-none block w-full text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-min-salary" name="min_salary" type="number" placeholder="5000000" value="{{ $job ? $job->min_salary : '' }}">
                </div>
                <div class="w-full md:w-1/2 mb-6 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-max-salary">
                        Gaji Maksimal
                    </label>
                    <input class="appearance-none block w-full text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-max-salary" name="max_salary" type="number" placeholder="5000000" value="{{ $job ? $job->max_salary : '' }}">
                </div>
                <div class="w-full mb-6 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Tipe Pekerjaan
                    </label>
                    <select id="job_employment_type" name="job_employment_type" class="w-full py-3 px-4">
                        <option value="1">Full Time</option>
                        <option value="2">Part Time</option>
                    </select>
                </div>
                <div class="w-full mb-6 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Kategori
                    </label>
                    <select id="job_categories" name="job_categories[]" class="w-full py-3 px-4" multiple="multiple">
                        @foreach($categories as $item)
                        <option value="{{ $item->categories_id }}">{{ $item->categories_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="w-full mb-6 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Pendidikan
                    </label>
                    <select id="job_educations" name="job_educations[]" class="w-full py-3 px-4" multiple="multiple">
                        @foreach($educations as $item)
                        <option value="{{ $item->education_id }}">{{ $item->education_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="w-full mb-6 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        industri
                    </label>
                    <select id="job_industries" name="job_industries[]" class="w-full py-3 px-4" multiple="multiple">
                    </select>
                </div>
                <div class="w-full mb-6 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" multiple="multiple">
                        Lokasi
                    </label>
                    <div class="form-group">
                        <select id="job_locations" name="job_locations[]" class="w-full py-3 px-4" multiple="multiple">
                        </select>
                    </div>
                </div>
                <div class="w-full mb-6 px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Spesialisasi
                    </label>
                    <div class="form-group">
                        <select id="job_specialization" name="job_specialization[]" class="w-full py-3 px-4" multiple="multiple">
                        </select>
                    </div>
                </div>
            </div>
            <div class="inline-block mr-2 mt-2">
                <button type="submit" class="focus:outline-none text-white text-sm py-2.5 px-5 rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg">Submit</button>
            </div>
        </form>
    </div>
<!-- </div> -->
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        ClassicEditor
            .create(document.querySelector('#test'), {
                toolbar: ['bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote'],
            })
            .catch(error => {
                console.error(error);
            });

        $('#job_categories').select2({
            dropdownPosition: 'below',
            placeholder: "Kategori Pekerjaan"
        });

        $('#job_educations').select2({
            dropdownPosition: 'below',
            placeholder: "Kategori Pendidikan"
        });

        $('#job_employment_type').select2({
            dropdownPosition: 'below',
            placeholder: "Tipe Pekerjaan"
        });

        $('#job_industries').select2({
            placeholder: "Choose tags...",
            minimumInputLength: 2,
            debug: true,
            ajax: {
                url: "{{ route('select_industries') }}",
                dataType: 'json',
                data: function(params) {
                    return {
                        term: params.term || '',
                        page: params.page || 1
                    }
                },
                // processResults: function(data, params) {
                //     params.page = params.page || 1;
                //     return {
                //         results: data.results,
                //         pagination: {
                //             more: (params.page * 10) < data.count_filtered
                //         }
                //     };
                // },
                cache: true,
            }
        });

        $('#job_specialization').select2({
            placeholder: "Choose tags...",
            minimumInputLength: 2,
            debug: true,
            ajax: {
                url: "{{ route('select_specialization') }}",
                dataType: 'json',
                data: function(params) {
                    return {
                        term: params.term || '',
                        page: params.page || 1
                    }
                },
                cache: true,
            }
        });

        $('#job_locations').select2({
            placeholder: "Choose tags...",
            minimumInputLength: 2,
            debug: true,
            ajax: {
                url: "{{ route('select_region') }}",
                dataType: 'json',
                data: function(params) {
                    return {
                        term: params.term || '',
                        page: params.page || 1
                    }
                },
                // processResults: function(data, params) {
                //     params.page = params.page || 1;
                //     return {
                //         results: data.results,
                //         pagination: {
                //             more: (params.page * 10) < data.count_filtered
                //         }
                //     };
                // },
                cache: true,
            }
        });

        //PreSelected Select2
        if (typeof laravel.specialization_sub_id !== undefined) {
            laravel.specialization_sub_id.forEach(data => {
                var newOption = new Option(data.text, data.id, true, true);
                $('#job_specialization').append(newOption).trigger('change');
            });
        }
        if (typeof laravel.region_id !== undefined) {
            laravel.region_id.forEach(data => {
                var newOption = new Option(data.text, data.id, true, true);
                $('#job_locations').append(newOption).trigger('change');
            });
        }
        if (typeof laravel.industries_id !== undefined) {
            laravel.industries_id.forEach(data => {
                var newOption = new Option(data.text, data.id, true, true);
                $('#job_industries').append(newOption).trigger('change');
            });
        }
        if (typeof laravel.categories_id !== undefined) {
            laravel.categories_id.forEach(data => {
                var newOption = new Option(data.text, data.id, true, true);
                $('#job_categories').append(newOption).trigger('change');
            });
        }
        if (typeof laravel.education_id !== undefined) {
            laravel.education_id.forEach(data => {
                var newOption = new Option(data.text, data.id, true, true);
                $('#job_educations').append(newOption).trigger('change');
            });
        }
    });
</script>
@endsection