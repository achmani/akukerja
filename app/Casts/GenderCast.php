<?php

namespace App\Casts;


use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class GenderCast implements CastsAttributes
{

    public function get($model, string $key, $value, array $attributes)
    {
        if($value == 1){
            return "Pria";
        }else if($value == 2){
            return "Wanita";
        }
        return "";
    }
    
    public function set($model, string $key, $value, array $attributes)
    {
       return $value;
    }


}