<?php

namespace App\Casts;


use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class BooleanCast implements CastsAttributes
{

    public function get($model, string $key, $value, array $attributes)
    {
        if($value == 0){
            return false;
        }else {
            return true;
        }
    }
    
    public function set($model, string $key, $value, array $attributes)
    {
       return $value;
    }


}