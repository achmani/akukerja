<?php

namespace App\Scopes;

/** Carbon */
use Illuminate\Support\Carbon;

/** Auth */
use Illuminate\Support\Facades\Auth;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class JobScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {   
        if (Auth::guard('company')->user()) {
            $builder->where('jobs.company_id', Auth::guard('company')->user()->company_id);
        }else{
            //$builder->where('jobs.job_status', 1)->where('jobs.job_expired','>=',Carbon::now()->format("Y-m-d"));
        }
        $builder->orderBy('jobs.job_expired', 'DESC');
    }
}