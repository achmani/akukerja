<?php

namespace App\Jobs;

use App\Mail\ResetPasswordEmail;
use Illuminate\Support\Facades\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendEmailResetPassword implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public $email;
    public $name;
    public $link;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $name, $link)
    {
        $this->email = $email;
        $this->name = $name;
        $this->link = $link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
         Mail::to($this->email)->send(new ResetPasswordEmail($this->name,$this->link));
    }
}
