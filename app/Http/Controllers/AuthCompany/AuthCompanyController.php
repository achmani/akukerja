<?php

namespace App\Http\Controllers\AuthCompany;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Auth\LoginController as DefaultLoginController;

use App\Models\UserCompany;
use App\Models\Companies;

class AuthCompanyController extends DefaultLoginController
{

    protected $redirectTo = '/dashboard';

    public function __construct()
    {
        $this->middleware('guest:company')->except('logout');
    }

    public function index()
    {
        return view('auth_company/login');
    }

    public function viewSignup(Request $request)
    {
        return view('auth_company/register');
    }

    public function signup(Request $request){
        $validatedData = $request->validate([
            'email' => 'email|required|unique:users_company',
            'company_name' => 'required',
            'password' => 'required|confirmed'
        ]);

        try{
            DB::beginTransaction();
            $company = new Companies();
            $company->company_name = $request->company_name;
            $company->company_address = "-";
            $company->company_mail = "-";
            $company->company_phone = "-";
            $company->company_industry = 0;
            $company->company_processing_time = 0;
            $company->company_benefits = "-";
            $company->company_min_employee = 0;
            $company->company_max_employee = 0;
            $company->company_overview = "-";
            $company->company_image = "-";
            $company->company_thumbnail = "-";
            $company->company_status = 0;
            $company->save();

            $user = new UserCompany();
            $user->email = $request->email;
            $user->name = $request->name;
            $user->company_id = $company->company_id;
            $user->password = bcrypt($request->password);   
            $user->save();
            DB::commit();

            $credentials = $request->only('email', 'password');

            if (Auth::guard('company')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
                return redirect()->intended('/dashboard');
            }
    
            return back()->withErrors([
                'email' => 'The provided credentials do not match our records.',
            ]);

        }catch (\Exception $e){
            \Log::debug($e);
            DB::rollback();
        }
    }

    public function username()
    {
        return 'email';
    }

    protected function guard()
    {
        return Auth::guard('company');
    }

}
