<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Job\Jobs;
use App\Models\Applicant\Bookmark;

class BookmarkController extends Controller
{
    public function setBookmark(Request $request){
        $user  = $request->user();
        $data = Bookmark::where('user_id',$user->id)->where('job_id',$request->id)->first();
        if(!$data){
            $bookmark = new Bookmark();
            $bookmark->user_id = $user->id;
            $bookmark->job_id = $request->id;
            $bookmark->save();
        }else{
            return response()->json([
                'status' => 500,
                'messages' => 'Failed. Data has been bookmarked',
            ], 500);
        }
        return response()->json([
            'status' => 200,
            'messages' => 'Success',
        ], 200);
    }

    public function getBookmark(Request $request){
        
        $user  = $request->user();
        $data = Jobs::join('users_bookmark','users_bookmark.job_id','jobs.job_id')->with(['companies','categories','regions'])->where('users_bookmark.user_id',$user->id);
        $data = $data->orderBy("jobs.job_id");
        $data = $data->select('jobs.job_id as job_id','company_id','job_title','job_posted','min_salary','max_salary')->orderBy("jobs.job_id")->get();     
        foreach ($data as $key => $value) {
            $temp = '';
            if(count($value->regions) <= 0){
                $temp = "";
            }
            else if(count($value->regions) > 1){
                $temp = "Multiple Location";
            }else{
                // $temp = "-";
                $temp = $value->regions[0]->name;
            }
            $human_readable = $value->job_posted->diffForHumans();
                        $data[$key]->region = $temp;
            $data[$key]->salary = "Rp.".number_format($data[$key]->min_salary,0,",",".")." - Rp.".number_format($data[$key]->max_salary,0,",",".");
            $data[$key]->release_date = $human_readable;
            if($value->companies){
                $data[$key]->company_name = $value->companies->company_name;
                $data[$key]->img = asset($value->companies->company_image);
            }else{
                $data[$key]->company_name = "";
            }
            unset($data[$key]->categories);
            unset($data[$key]->regions);
            unset($data[$key]->company_id);
            unset($data[$key]->companies);
            unset($data[$key]->job_posted);
            unset($data[$key]->min_salary);
            unset($data[$key]->max_salary);

        }

        return response()->json($data);
    }

    public function deleteBookmark(Request $request){
        $user  = $request->user();
        $data = Bookmark::where('user_id',$user->id)->where('job_id',$request->id)->first();
        if($data){
            $data = Bookmark::where('user_id',$user->id)->where('job_id',$request->id)->delete();
        }else{
            return response()->json([
                'status' => 500,
                'messages' => 'Failed. Data not found',
            ], 500);
        }
        return response()->json([
            'status' => 200,
            'messages' => 'Success',
        ], 200);
    }
}
