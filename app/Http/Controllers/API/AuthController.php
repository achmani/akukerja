<?php

namespace App\Http\Controllers\API;


use App\Jobs\SendEmail;
use App\Jobs\SendEmailResetPassword;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Firebase\Auth\Token\Exception\InvalidToken;
use Illuminate\Support\Carbon;

use Illuminate\Support\Facades\Log;

use App\Models\User;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed'
        ]);
        $validatedData['name'] = $request->first_name;
        $validatedData['back_name'] = $request->last_name;
        $validatedData['FirebaseUID'] = '-';
        $validatedData['ProviderId'] = '-';

        $name = $request->first_name." ".$request->lastname;

        $validatedData['password'] = bcrypt($request->password);

        $user = User::create($validatedData);

        $accessToken = $user->createToken('authToken')->accessToken;
        
        SendEmail::dispatch($request->email, $name, $request->password);

        return response(['user' => $user, 'access_token' => $accessToken]);
    }

    public function login_email(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'email|required|exists:users,email',
            'password' => 'required'
        ]);
        $user = User::where('email', $request->email)->first();
        if (Hash::check($request->password, $user->password)) {
            // Once we got a valid user model
            // Create a Personnal Access Token
            $tokenResult = $user->createToken('Personal Access Token');

            // Store the created token
            $token = $tokenResult->token;

            // Add a expiration date to the token
            $token->expires_at = Carbon::now()->addWeeks(1);

            // Save the token to the user
            $token->save();

            // Return a JSON object containing the token datas
            // You may format this object to suit your needs
            
            return response()->json([
                'id' => $user->id,
                'initialization' => $user->initialization,
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ]);
            
        }

        return response()->json([
            'message' => 'Password Invalid'
        ], 401);
    }

    // public function login(Request $request)
    // {
    //     $loginData = $request->validate([
    //         'email' => 'email|required',
    //         'password' => 'required'
    //     ]);

    //     if (!Auth::attempt($loginData)) {
    //         return response(['message' => 'Invalid Credentials']);
    //     }

    //     $accessToken = Auth::user()->createToken('authToken')->accessToken;

    //     return response(['user' => auth()->user(), 'access_token' => $accessToken]);
    // }

    public function login(Request $request)
    {
        return view('welcome');
    }

    // Please, note that this code is just an extract of loginController.php
    // You should ensure that all datas received are valid and secure.

    public function login_firebase(Request $request)
    {

        // Launch Firebase Auth
        $auth = app('firebase.auth');
        // Retrieve the Firebase credential's token
        $idTokenString = $request->input('Firebasetoken');
        $user = $request->input("User");

        try { // Try to verify the Firebase credential token with Google

            $verifiedIdToken = $auth->verifyIdToken($idTokenString);
        } catch (\InvalidArgumentException $e) { // If the token has the wrong format

            return response()->json([
                'message' => 'Unauthorized - Can\'t parse the token: ' . $e->getMessage()
            ], 401);
        } catch (InvalidToken $e) { // If the token is invalid (expired ...)

            return response()->json([
                'message' => 'Unauthorized - Token is invalide: ' . $e->getMessage()
            ], 401);
        }

        // Retrieve the UID (User ID) from the verified Firebase credential's token
        $uid = $verifiedIdToken->getClaim('sub');

        $users = $auth->getUsers([$uid]);

        // Retrieve the user model linked with the Firebase UID
        $user = User::where('email', $users[$uid]->email)->first();
        if (!$user) {
            $random_password = Str::random(8);
            $hashed_random_password = bcrypt($random_password);
            $user = new User();
            $user->FirebaseUID = $uid;
            $user->name = $users[$uid]->displayName;
            $user->back_name = $users[$uid]->displayName;
            $user->email = $users[$uid]->email;
            $user->password = $hashed_random_password;
            $user->PhotoUrl = $users[$uid]->photoUrl;
            $user->ProviderId = $users[$uid]->providerData[0]->providerId;
            $user->email_verified_at = $users[$uid]->metadata->createdAt;
            $user->save();
            SendEmail::dispatch($users[$uid]->email, $users[$uid]->displayName, $random_password);
        } else {
            $user->FirebaseUID = $uid;
            $user->ProviderId = $users[$uid]->providerData[0]->providerId;
            $user->email_verified_at = $users[$uid]->metadata->createdAt;
            $user->save();
        }

        // Here you could check if the user model exist and if not create it
        // For simplicity we will ignore this step

        // Once we got a valid user model
        // Create a Personnal Access Token
        $tokenResult = $user->createToken('Personal Access Token');

        // Store the created token
        $token = $tokenResult->token;

        // Add a expiration date to the token
        $token->expires_at = Carbon::now()->addWeeks(1);

        // Save the token to the user
        $token->save();

        // Return a JSON object containing the token datas
        // You may format this object to suit your needs
        return response()->json([
            'id' => $user->id,
            'initialization' => $user->initialization,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    public function reset_password(Request $request){
        
        //Get user data
        $email = $request->email;
        $user = User::where('email',$email)->first();
        $now = Carbon::now()->timestamp;


        // Store a string into the variable which 
        // need to be Encrypted 
        //$simple_string = md5("G3n3sys123-").sha1($serial).sha1("-witonods4123").md5($serial); 
        $simple_string = $user->email."|".$user->ProviderId.'|'.$user->name.'|'.$user->back_name.'|'.$now;
        
        // Store cipher method 
        $ciphering = "BF-CBC"; 
        
        // Use OpenSSl encryption method 
        $iv_length = openssl_cipher_iv_length($ciphering); 
        $options = 0; 
        
        // Use random_bytes() function which gives 
        // randomly 16 digit values 
        #$encryption_iv = random_bytes($iv_length);
        $encryption_iv = "�/$%θ";
        
        // Alternatively, we can use any 16 digit 
        // characters or numeric for iv 
        $encryption_key = openssl_digest(php_uname(), 'SHA1', TRUE); 
        
        // Encryption of string process starts 
        $encryption = openssl_encrypt($simple_string, $ciphering, 
                $encryption_key, $options, $encryption_iv); 
        
        SendEmailResetPassword::dispatch($user->email, $user->name, route('password.reset')."?encrypt=".base64_encode($encryption));
        return $encryption;
    }

}
