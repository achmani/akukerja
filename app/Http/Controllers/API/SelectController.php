<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Master\SpecializationSubMaster;
use App\Models\Location\Regency;
use App\Models\Master\IndustriesMaster;


class SelectController extends Controller
{

    public function selectSpecialization(Request $request)
    {
        // $search = $request->search;
        // $data = SpecializationSubMaster::where('specialization_sub_name', 'LIKE', "%" . $search . "%")->select(['specialization_sub_id as id', 'specialization_sub_name as name as text'])->get();
        // return response()->json($data);

        $page = $request->page;
        $resultCount = 10;
        $offset = ($page - 1) * $resultCount;

        $search = trim($request->term);

        // if (empty($term)) {
        //     return response()->json([]);
        // }

        $data = SpecializationSubMaster::where('specialization_sub_name', 'LIKE', "%" . $search . "%")->skip($offset)->take($resultCount)->select(['specialization_sub_id as id', 'specialization_sub_name as text'])->get();
        $count = SpecializationSubMaster::where('specialization_sub_name', 'LIKE', "%" . $search . "%")->count();
        $endCount = $offset + $resultCount;
        $morePages = $count > $endCount;

        $results = array(
            "results" => $data,
            "count_filtered" => $count,
            "pagination" => array(
                "more" => $morePages
            )
        );

        return response()->json($results);
    }

    public function selectRegion(Request $request)
    {
        // $search = $request->search;
        // $data = SpecializationSubMaster::where('specialization_sub_name', 'LIKE', "%" . $search . "%")->select(['specialization_sub_id as id', 'specialization_sub_name as name as text'])->get();
        // return response()->json($data);

        $page = $request->page;
        $resultCount = 10;
        $offset = ($page - 1) * $resultCount;

        $search = trim($request->term);

        // if (empty($term)) {
        //     return response()->json([]);
        // }

        $data = Regency::where('name', 'LIKE', "%" . $search . "%")->skip($offset)->take($resultCount)->select(['id as id', 'name as text'])->get();
        $count = Regency::where('name', 'LIKE', "%" . $search . "%")->count();
        $endCount = $offset + $resultCount;
        $morePages = $count > $endCount;

        $results = array(
            "results" => $data,
            "count_filtered" => $count,
            "pagination" => array(
                "more" => $morePages
            )
        );

        return response()->json($results);
    }

    public function selectIndustries(Request $request)
    {
        // $search = $request->search;
        // $data = SpecializationSubMaster::where('specialization_sub_name', 'LIKE', "%" . $search . "%")->select(['specialization_sub_id as id', 'specialization_sub_name as name as text'])->get();
        // return response()->json($data);

        $page = $request->page;
        $resultCount = 10;
        $offset = ($page - 1) * $resultCount;

        $search = trim($request->term);

        // if (empty($term)) {
        //     return response()->json([]);
        // }

        $data = IndustriesMaster::where('industries_name', 'LIKE', "%" . $search . "%")->skip($offset)->take($resultCount)->select(['industries_id as id', 'industries_name as text'])->get();
        $count = IndustriesMaster::where('industries_name', 'LIKE', "%" . $search . "%")->count();
        $endCount = $offset + $resultCount;
        $morePages = $count > $endCount;

        $results = array(
            "results" => $data,
            "count_filtered" => $count,
            "pagination" => array(
                "more" => $morePages
            )
        );

        return response()->json($results);
    }

}
