<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Job\Jobs;
use App\Models\Applicant\Apply;

class ApplyController extends Controller
{
    public function setApply(Request $request)
    {
        $user  = $request->user();
        $data = Apply::where('user_id', $user->id)->where('job_id', $request->id)->first();
        if (!$data) {
            $Apply = new Apply();
            $Apply->user_id = $user->id;
            $Apply->job_id = $request->id;
            $Apply->additional_info = $request->additional_info;
            $Apply->save();
        } else {
            return response()->json([
                'status' => 500,
                'messages' => 'Failed. Data has been Applyed',
            ], 500);
        }
        return response()->json([
            'status' => 200,
            'messages' => 'Success',
        ], 200);
    }

    public function getApply(Request $request)
    {

        $user  = $request->user();
        $data = Jobs::join('users_apply', 'users_apply.job_id', 'jobs.job_id')->with(['companies', 'categories', 'regions'])->where('users_apply.user_id', $user->id);
        $data = $data->orderBy("jobs.job_id");
        $data = $data->select('jobs.job_id as job_id', 'company_id', 'job_title', 'job_posted', 'min_salary', 'max_salary', 'users_apply.created_at as job_applied')->orderBy("jobs.job_id")->get();
        foreach ($data as $key => $value) {
            $temp = '';
            if (count($value->regions) <= 0) {
                $temp = "";
            } else if (count($value->regions) > 1) {
                $temp = "Multiple Location";
            } else {
                // $temp = "-";
                $temp = $value->regions[0]->name;
            }
            $human_readable = $value->job_applied->diffForHumans();
            $data[$key]->region = $temp;
            $data[$key]->received = 0;
            $data[$key]->salary = "Rp." . number_format($data[$key]->min_salary, 0, ",", ".") . " - Rp." . number_format($data[$key]->max_salary, 0, ",", ".");
            $data[$key]->applied_date = $human_readable;
            if ($value->companies) {
                $data[$key]->company_name = $value->companies->company_name;
                $data[$key]->img = asset($value->companies->company_image);
            } else {
                $data[$key]->company_name = "";
            }
            unset($data[$key]->categories);
            unset($data[$key]->regions);
            unset($data[$key]->company_id);
            unset($data[$key]->companies);
            unset($data[$key]->job_posted);
            unset($data[$key]->min_salary);
            unset($data[$key]->max_salary);
        }

        return response()->json($data);
    }
}
