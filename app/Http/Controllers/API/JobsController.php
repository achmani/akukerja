<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

use App\Models\Job\Jobs;
use App\Models\Applicant\Regions as ApplicantRegions;
use App\Models\Applicant\Categories as ApplicantCategories;
use App\Models\Location\Province;
use App\Models\Location\Regency;

class JobsController extends Controller
{

    public function list(Request $request)
    {
        $results = $request->results;
        $page = $request->page;
        $headerAuthorization = $request->header('Authorization');
        if ($headerAuthorization) {
            $user = auth()->guard('api')->user();
        }

        if (isset($user)) {
            $applicantCategories = ApplicantCategories::where("user_id", $user->id)->pluck("categories_id")->toArray();
            $applicantRegions = ApplicantRegions::where("user_id", $user->id)->pluck("province_id")->toArray();
            $salary = $user->salary;
            $regencies = Regency::whereIn('province_id', $applicantRegions)->pluck('id')->toArray();
        }

        if (isset($user)) {
            $data = Jobs::with(['companies', 'categories', 'regions'])
                ->whereHas('regions', function ($query) use ($regencies) {
                    $query->whereIn('region_id', $regencies);
                })
                ->whereHas('categories', function ($query) use ($applicantCategories) {
                    $query->whereIn('jobs_categories.categories_id', $applicantCategories);
                });
        } else {
            $data = Jobs::with(['companies', 'categories', 'regions']);
        }

        $data = $data->select('jobs.job_id', 'company_id', 'job_title', 'job_posted', 'min_salary', 'max_salary')
            ->skip($page * $results)->take($results)->orderBy("jobs.job_id","DESC")->get();

        foreach ($data as $key => $value) {
            $temp = '';
            if (count($value->regions) <= 0) {
                $temp = "";
            } else if (count($value->regions) > 1) {
                // $temp = "Multiple Location";
                $temp = ucwords(strtolower(implode (", ", $value->regions->pluck('name')->toArray())));
            } else {
                $temp = ucwords(strtolower($value->regions[0]->name));
            }
            $human_readable = $value->job_posted->diffForHumans();
            $data[$key]->region = $temp;
            $data[$key]->release_date = $human_readable;
            if ($value->companies) {
                $data[$key]->company_name = $value->companies->company_name;
                $data[$key]->img = asset($value->companies->company_image);
            } else {
                $data[$key]->company_name = "";
            }

            $data[$key]->bookmark = 0;
            $data[$key]->apply = 0;

            if (isset($user)) {
                if($data[$key]->min_salary != $data[$key]->max_salary){
                    $data[$key]->salary = "Rp." . number_format($data[$key]->min_salary, 0, ",", ".") . " - Rp." . number_format($data[$key]->max_salary, 0, ",", ".");
                }else{
                    $data[$key]->salary = "Rp." . number_format($data[$key]->min_salary, 0, ",", ".");                    
                }
            } else {
                $data[$key]->salary = "";
            }

            if (isset($user)) {
                $temp = $value->bookmarked->pluck("user_id")->toArray();
                if ((in_array($user->id, $temp))) {
                    $data[$key]->bookmark = 1;
                }

                $temp = $value->applied->pluck("user_id")->toArray();
                if ((in_array($user->id, $temp))) {
                    $data[$key]->apply = 1;
                }
            }

            unset($data[$key]->categories);
            unset($data[$key]->regions);
            unset($data[$key]->applied);
            unset($data[$key]->bookmarked);
            unset($data[$key]->company_id);
            unset($data[$key]->companies);
            unset($data[$key]->job_posted);
            unset($data[$key]->min_salary);
            unset($data[$key]->max_salary);
        }

        return response()->json($data);
    }

    public function search(Request $request)
    {
        $results = $request->results;
        $page = $request->page;
        $headerAuthorization = $request->header('Authorization');
        if ($headerAuthorization) {
            $user = auth()->guard('api')->user();
        }
        $categories_id = array_filter(explode(',', $request->categories_id));
        $keyword = $request->keyword;

        $data = Jobs::search($keyword)->with(['companies', 'categories', 'regions']);
        if($categories_id){
            $data = $data->whereHas('categories', function ($query) use ($categories_id) {
                $query->whereIn('jobs_categories.categories_id', $categories_id);
            });
        }
        $data = $data->select('jobs.job_id', 'company_id', 'job_title', 'job_posted', 'min_salary', 'max_salary')
            ->skip($page * $results)->take($results)->orderBy("jobs.job_id","DESC")->get();

        foreach ($data as $key => $value) {
            $temp = '';
            if (count($value->regions) <= 0) {
                $temp = "";
            } else if (count($value->regions) > 1) {
                // $temp = "Multiple Location";
                $temp = ucwords(strtolower(implode (", ", $value->regions->pluck('name')->toArray())));
            } else {
                $temp = ucwords(strtolower($value->regions[0]->name));
            }
            $human_readable = $value->job_posted->diffForHumans();
            $data[$key]->region = $temp;
            $data[$key]->release_date = $human_readable;
            if ($value->companies) {
                $data[$key]->company_name = $value->companies->company_name;
                $data[$key]->img = asset($value->companies->company_image);
            } else {
                $data[$key]->company_name = "";
            }

            $data[$key]->bookmark = 0;
            $data[$key]->apply = 0;

            if (isset($user)) {
                if($data[$key]->min_salary != $data[$key]->max_salary){
                    $data[$key]->salary = "Rp." . number_format($data[$key]->min_salary, 0, ",", ".") . " - Rp." . number_format($data[$key]->max_salary, 0, ",", ".");
                }else{
                    $data[$key]->salary = "Rp." . number_format($data[$key]->min_salary, 0, ",", ".");                    
                }
            } else {
                $data[$key]->salary = "";
            }

            if (isset($user)) {
                $temp = $value->bookmarked->pluck("user_id")->toArray();
                if ((in_array($user->id, $temp))) {
                    $data[$key]->bookmark = 1;
                }

                $temp = $value->applied->pluck("user_id")->toArray();
                if ((in_array($user->id, $temp))) {
                    $data[$key]->apply = 1;
                }
            }

            unset($data[$key]->categories);
            unset($data[$key]->regions);
            unset($data[$key]->applied);
            unset($data[$key]->bookmarked);
            unset($data[$key]->company_id);
            unset($data[$key]->companies);
            unset($data[$key]->job_posted);
            unset($data[$key]->min_salary);
            unset($data[$key]->max_salary);
        }

        return response()->json($data);
    }

    public function detail(Request $request)
    {
        $headerAuthorization = $request->header('Authorization');
        if ($headerAuthorization) {
            $user = auth()->guard('api')->user();
        }

        $id = $request->id;
        $data = Jobs::select('job_id', 'job_title', 'job_posted', 'job_description', 'company_name', 'company_address', 'company_mail', 'company_phone', 'company_processing_time', 'company_min_employee', 'company_max_employee', 'company_overview', 'company_image as image_url', 'company_status','min_salary','max_salary')->join('companies', 'companies.company_id', 'jobs.company_id')->with(['categories'])->with('regions')->where('jobs.job_id', $id)->first();

        $temp = [];
        foreach ($data->categories as $item) {
            $temp[] = $item['categories_name'];
        }
        unset($data->categories);
        $data->categories = $temp;

        $temp = [];
        foreach ($data->regions as $item) {
            $temp[] = $item['name'];
        }

        $data->bookmark = 0;
        $data->apply = 0;

        if (isset($user)) {
            if($data->min_salary != $data->max_salary){
                $data->salary = "Rp." . number_format($data->min_salary, 0, ",", ".") . " - Rp." . number_format($data->max_salary, 0, ",", ".");
            }else{
                $data->salary = "Rp." . number_format($data->min_salary, 0, ",", ".");                    
            }
        } else {
            $data->salary = "";
        }
        if (isset($user)) {
            $temp = $data->bookmarked->pluck("user_id")->toArray();
            if ((in_array($user->id, $temp))) {
                $data->bookmark = 1;
            }

            $temp = $data->applied->pluck("user_id")->toArray();
            if ((in_array($user->id, $temp))) {
                $data->apply = 1;
            }
        }

        if($data->image_url){
            $data->image_url = asset($data->image_url);
        }

        // if ($data->companies) {
        //     $data->image_url = asset($data->companies->company_image);
        // } else {
        //     $data->image_url = "";
        // }

        unset($data->regions);
        unset($data->min_salary);
        unset($data->max_salary);
        unset($data->applied);
        unset($data->bookmarked);
        $data->job_posted = Carbon::parse($data->job_posted)->format('d M Y');
        $data->regions = $temp;

        return response()->json($data);
    }

    private function findNearestLocation($latitude, $longitude, $radius = 100000000000)
    {
        /*
         * using eloquent approach, make sure to replace the "Restaurant" with your actual model name
         * replace 6371000 with 6371 for kilometer and 3956 for miles
         */
        $regency = Regency::selectRaw("id, province_id, name, latitude, longitude,
                         ( 6371000 * acos( cos( radians(?) ) *
                           cos( radians( latitude ) )
                           * cos( radians( longitude ) - radians(?)
                           ) + sin( radians(?) ) *
                           sin( radians( latitude ) ) )
                         ) AS distance", [$latitude, $longitude, $latitude])
            ->where('active', '=', 1)
            ->having("distance", "<", $radius)
            ->orderBy("distance", 'asc')
            ->offset(0)
            ->limit(1)
            ->first();

        return $regency;
    }

    public function getNearestLocation(Request $request)
    {

        $headerAuthorization = $request->header('Authorization');

        if ($headerAuthorization) {
            return auth()->guard('api')->user();
        }

        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $regency = $this->findNearestLocation($latitude, $longitude);
        $province = Province::find($regency->province_id);
        return $province;
    }
}
