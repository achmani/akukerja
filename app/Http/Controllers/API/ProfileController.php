<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Models\User;
use App\Models\Companies;
use App\Models\Categories;
use App\Models\Location\Regency;
use App\Models\Location\Province;
use App\Models\Master\Position;
use App\Models\Master\Majors;
use App\Models\Master\IndustriesMaster;
use App\Models\Master\EducationMaster;
use App\Models\Master\SpecializationMaster;
use App\Models\Master\SpecializationSubMaster;
use App\Models\Applicant\Experience;
use App\Models\Applicant\Expertise as UserExpertise;
use App\Models\Applicant\Language as UserLanguage;
use App\Models\Applicant\CompaniesExperience;
use App\Models\Applicant\Regions as UserRegions;
use App\Models\Applicant\Categories as UserCategories;
use App\Models\Applicant\Education as UserEducations;

class ProfileController extends Controller
{

    public function getProfile(Request $request)
    {
        $user  = $request->user();
        $user = User::where('id', $user->id)->select(["*","gender as gender_name"])->first();
        return $user;
    }

    public function setProfile(Request $request)
    {
        $data = [];
        $data['name'] = $request->name;
        $data['birth_date'] = $request->birth_date;
        $data['back_name'] = $request->back_name;
        $data['phone_number'] = $request->phone_number;
        $data['gender'] = $request->gender;
        $data['salary'] = $request->salary;

        $user  = $request->user();
        $user->update($data);

        return response()->json([
            'status' => 200,
            'messages' => 'Success',
        ], 200);
    }

    public function listEducations(Request $request)
    {
        $keyword = $request->keyword;
        $data = EducationMaster::where('education_name', 'LIKE', '%' . $keyword . '%')->select('education_id as id', 'education_name as name')->get();
        return response()->json($data);
    }

    public function listMajors(Request $request)
    {
        $keyword = $request->keyword;
        $data = Majors::where('majors_name', 'LIKE', '%' . $keyword . '%')->select('majors_id as id', 'majors_name as name')->get();
        return response()->json($data);
    }

    public function setLanguage(Request $request)
    {
        $data = $request->all();
        $user  = $request->user();
        $expertise = UserLanguage::where('user_id', $user->id)->delete();
        $sequence = 0;
        foreach ($data as $key => $value) {
            $language_new = new UserLanguage();
            $language_new->user_id = $user->id;
            $language_new->user_language = $value['language'];
            $language_new->user_language_seq = $sequence++;
            $language_new->user_language_write = $value['ability_language_write'];
            $language_new->user_language_spoke = $value['ability_language_spoke'];
            $language_new->user_language_is_first = $value['is_first_language'];
            $language_new->save();
        }
        return response()->json([
            'status' => 200,
            'messages' => 'Success',
        ], 200);
    }

    public function getLanguage(Request $request)
    {
        $user  = $request->user();
        $language = UserLanguage::where('user_id', $user->id)->select(['user_language as language', 'user_language_write as ability_language_write', 'user_language_spoke as ability_language_spoke', 'user_language_is_first as is_first_language'])->get();
        return response()->json($language);
    }

    public function setExpertise(Request $request)
    {
        $data = $request->all();
        $user  = $request->user();
        $expertise = UserExpertise::where('user_id', $user->id)->delete();
        $sequence = 0;
        foreach ($data as $key => $value) {
            $expertise_new = new UserExpertise();
            $expertise_new->user_id = $user->id;
            $expertise_new->user_expertise_seq = $sequence++;
            $expertise_new->user_expertise_name = $value['skill_description'];
            $expertise_new->user_expertise_level = $value['level_skill'];
            $expertise_new->save();
        }
        return response()->json([
            'status' => 200,
            'messages' => 'Success',
        ], 200);
    }

    public function getExpertise(Request $request)
    {
        $user  = $request->user();
        $expertise = UserExpertise::where('user_id', $user->id)->select(['user_expertise_name as skill_description', 'user_expertise_level as level_skill'])->get();
        return response()->json($expertise);
    }

    public function setEducation(Request $request)
    {
        if ($request->method() == "PUT") {
            $educations = UserEducations::find($request->id);
            if (!$educations) {
                \Log::debug("masuk sini");
                return response()->json([
                    'status' => 500,
                    'messages' => 'Failed. Data not Found',
                ], 500);
            }
        }
        DB::beginTransaction();
        try {
            $now = Carbon::now('Asia/Jakarta')->toDateTimeString();
            $data = [];
            $data['user_id'] = $request->user()->id;
            $data['education_id'] = $request->education_id;
            $data['major_id'] = $request->field_studies_id;
            $data['institution'] = $request->institution;
            $data['user_education_year'] = $request->year_education;
            $data['user_education_month'] = $request->month_id;
            $data['user_education_value'] = $request->value;
            $data['user_education_desc'] = $request->additional_info;
            if ($request->method() == "POST") {
                $data['created_at'] = $now;
            }
            $data['updated_at'] = $now;
            if ($request->method() == "PUT") {
                $educations->update($data);
            } else {
                UserEducations::insert($data);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            \Log::debug($request->education_id);
        
            return response()->json([
                'status' => 500,
                'messages' => 'Failed',
            ], 500);
        }
        return response()->json([
            'status' => 200,
            'messages' => 'Success',
        ], 200);
    }

    public function getEducation(Request $request)
    {
        $user  = $request->user();
        $data = UserEducations::leftjoin("majors", "majors.major_id", "users_education.major_id")->leftjoin("educations", "educations.education_id", "users_education.education_id")->where("users_education.user_id", $user->id)
            ->select([
                'user_education_id as id',
                'institution',
                'majors.major_id as field_studies_id',
                'majors.major_name as field_studies_name',
                'educations.education_id as education_id',
                'educations.education_name as education_desc',
                'user_education_year as year_education',
                'user_education_month as month_id',
                'user_education_month as month_desc',
                'user_education_desc as additional_info',
                'user_education_value as value'
            ])
            ->get();

        $month_name = [
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember"
        ];

        foreach ($data as $key => $value) {
            $data[$key]->month_desc = $month_name[$data[$key]->month_desc - 1];
        }

        return response()->json($data);
    }

    public function deleteEducation(Request $request)
    {
        $educations = UserEducations::find($request->id);
        if (!$educations) {
            return response()->json([
                'status' => 500,
                'messages' => 'Failed. Data not Found',
            ], 500);
        }
        $educations->delete();
        return response()->json([
            'status' => 200,
            'messages' => 'Success',
        ], 200);
    }

    public function getExperience(Request $request)
    {
        $user  = $request->user();
        $data = Experience::join('companies_experience', 'company_experience_id', 'user_experience_company')
            ->join('users', 'id', 'user_experience_user_id')
            ->join('industries', 'industries_id', 'user_experience_industry')
            ->join('position_level', 'position_level_id', 'user_experience_position')
            ->join('specializations_sub', 'specialization_sub_id', 'user_experience_specialization')
            ->join('specializations', 'specializations_sub.specialization_id', 'specializations.specialization_id')
            ->where('user_experience_user_id', $user->id)
            ->select([
                "user_experience_id as id",
                "users_experience.user_experience_title as job_title",
                "companies_experience.company_experience_id as company_id",
                "companies_experience.company_experience_name as company_name",
                "specializations.specialization_id as specialization_id",
                "specializations.specialization_name as specialization_name",
                "specializations_sub.specialization_sub_id as specialization_sub_id",
                "specializations_sub.specialization_sub_name as specialization_sub_name",
                "industries_id",
                "industries_name",
                "user_experience_start as month_start",
                "user_experience_start as year_start",
                "user_experience_end as month_end",
                "user_experience_end as year_end",
                "user_experience_desc as description",
                "user_experience_status as is_currently_work",
                "position_level_id",
                "position_level_name"
            ])
            ->get();

        $month_name = [
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember"
        ];

        foreach ($data as $key => $value) {
            $data[$key]->year_start = substr($data[$key]->year_start, 0, 4);
            $data[$key]->month_start = $month_name[ltrim(substr($data[$key]->month_start, 5, 2), '0') - 1];
            $data[$key]->year_end = substr($data[$key]->year_end, 0, 4);
            $data[$key]->month_end = $month_name[ltrim(substr($data[$key]->month_end, 5, 2), '0') - 1];
        }

        return response()->json($data);
    }

    public function detailExperience(Request $request)
    {
        $user  = $request->user();
        $data = Experience::join('companies_experience', 'company_experience_id', 'user_experience_company')
            ->join('users', 'id', 'user_experience_user_id')
            ->join('industries', 'industries_id', 'user_experience_industry')
            ->join('position_level', 'position_level_id', 'user_experience_position')
            ->join('specializations_sub', 'specialization_sub_id', 'user_experience_specialization')
            ->join('specializations', 'specializations_sub.specialization_id', 'specializations.specialization_id')
            ->where('user_experience_user_id', $user->id)
            ->where('user_experience_id', $request->id)
            ->select([
                "user_experience_id as id",
                "users_experience.user_experience_title as job_title",
                "companies_experience.company_experience_id as company_id",
                "companies_experience.company_experience_name as company_name",
                "specializations.specialization_id as specialization_id",
                "specializations.specialization_name as specialization_name",
                "specializations_sub.specialization_sub_id as specialization_sub_id",
                "specializations_sub.specialization_sub_name as specialization_sub_name",
                "industries_id",
                "industries_name",
                "user_experience_start as month_start",
                "user_experience_start as year_start",
                "user_experience_end as month_end",
                "user_experience_end as year_end",
                "user_experience_desc as description",
                "user_experience_status as is_currently_work",
                "position_level_id",
                "position_level_name"
            ])
            ->first();

        if ($data) {
            $data->year_start = substr($data->year_start, 0, 4);
            $data->month_start = ltrim(substr($data->month_start, 5, 2), '0');
            $data->year_end = substr($data->year_end, 0, 4);
            $data->month_end = ltrim(substr($data->month_end, 5, 2), '0');
        }

        return response()->json($data);
    }

    public function setExperience(Request $request)
    {
        DB::beginTransaction();
        if ($request->method() == "PUT") {
            $experience = Experience::find($request->id);
            if (!$experience) {
                return response()->json([
                    'status' => 500,
                    'messages' => 'Failed. Data not Found',
                ], 500);
            }
        }
        try {
            $data = [];
            $data['user_experience_user_id'] = $request->user()->id;
            $data['user_experience_title'] = $request->job_title;
            if ($request->company_id != "NEW") {
                $data['user_experience_company'] = $request->company_id;
            } else {
                $company = new CompaniesExperience();
                $company->company_experience_name = $request->company_name;
                $company->save();
                $data['user_experience_company'] = $company->company_experience_id;
            }
            $data['user_experience_specialization'] = $request->specialization_sub_id;
            $data['user_experience_start'] = $request->year_start . "-" . str_pad($request->month_start, 2, "0", STR_PAD_LEFT) . "-01";
            if ($request->year_end && $request->month_end) {
                $data['user_experience_end'] = $request->year_end . "-" . str_pad($request->month_end, 2, "0", STR_PAD_LEFT) . "-01";
            }
            $data['user_experience_industry'] = $request->industries_id;
            $data['user_experience_position'] = $request->position_level_id;
            $data['user_experience_desc'] = $request->description;
            $data['user_experience_status'] = $request->is_currently_work;
            if ($request->method() == "PUT") {
                $experience->update($data);
            } else {
                Experience::insert($data);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 500,
                'messages' => 'Failed',
            ], 500);
        }
        return response()->json([
            'status' => 200,
            'messages' => 'Success',
        ], 200);
    }

    public function deleteExperience(Request $request)
    {
        $experience = Experience::find($request->id);
        if (!$experience) {
            return response()->json([
                'status' => 500,
                'messages' => 'Failed. Data not Found',
            ], 500);
        }
        $experience->delete();
        return response()->json([
            'status' => 200,
            'messages' => 'Success',
        ], 200);
    }

    public function listProvince(Request $request)
    {
        $keyword = $request->keyword;
        if ($keyword) {
            $data = Province::where('name', 'LIKE', '%' . $keyword . '%')->select('id', 'name')->get();
        } else {
            $data = Province::select('id', 'name')->get();
        }

        return response()->json($data);
    }

    public function listCompanies(Request $request)
    {
        $keyword = $request->keyword;
        if ($keyword) {
            $data = Companies::where('company_name', 'LIKE', '%' . $keyword . '%')->where('company_address', 'LIKE', '%' . $keyword . '%')->select('company_id', 'company_name')->get();
        } else {
            $data = Companies::select('company_id', 'company_name')->get();
        }

        return response()->json($data);
    }

    public function listCompaniesExperience(Request $request)
    {
        $keyword = $request->keyword;
        if ($keyword) {
            $data = CompaniesExperience::where('company_experience_name', 'LIKE', '%' . $keyword . '%')->select('company_experience_id', 'company_name')->get();
        } else {
            $data = CompaniesExperience::select('company_experience_id as company_id', 'company_experience_name as company_name')->get();
        }

        return response()->json($data);
    }

    public function listRegency(Request $request)
    {
        $keyword = $request->keyword;
        $data = Regency::where('name', 'LIKE', '%' . $keyword . '%')->select('id', 'name')->get();
        return response()->json($data);
    }

    public function listIndustries(Request $request)
    {
        $keyword = $request->keyword;
        $data = IndustriesMaster::where('industries_name', 'LIKE', '%' . $keyword . '%')->select('industries_id', 'industries_name')->get();
        return response()->json($data);
    }

    public function listPosition(Request $request)
    {
        $keyword = $request->keyword;
        if ($keyword) {
            $data = Position::where('position_level_name', 'LIKE', '%' . $keyword . '%')->select('position_level_id as id', 'position_level_name as name')->get();
        } else {
            $data = Position::select('position_level_id as id', 'position_level_name as name')->get();
        }

        return response()->json($data);
    }

    public function listSpecialization(Request $request)
    {
        $keyword = $request->keyword;
        $data = SpecializationMaster::where('specialization_name', 'LIKE', '%' . $keyword . '%')->select('specialization_id', 'specialization_name')->get();
        return response()->json($data);
    }

    public function detailSpecialization(Request $request)
    {
        $id = $request->id;
        $keyword = $request->keyword;
        $data = SpecializationSubMaster::where('specialization_id', $id);
        if ($keyword) {
            $data = $data->where('specialization_sub_name', 'LIKE', '%' . $keyword . '%');
        }
        $data = $data->select('specialization_sub_id', 'specialization_sub_name')->get();
        return response()->json($data);
    }

    public function listCategories(Request $request)
    {
        $keyword = $request->keyword;

        if ($keyword) {
            $data = Categories::where('categories_name', 'LIKE', '%' . $keyword . '%')->select("categories_id", "categories_name", "categories_logo")->get();
        } else {
            $data = Categories::select("categories_id", "categories_name", "categories_logo")->get();
        }

        foreach ($data as $key => $value) {
            $value->categories_logo = url("app/images/categories/" . $value->categories_logo);
        }

        return response()->json($data);
    }

    public function initialization(Request $request)
    {
        $user = $request->user();
        $regions = $request->regions;
        $categories = $request->categories;
        $salary = $request->salary;

        DB::beginTransaction();
        $now = Carbon::now('Asia/Jakarta')->toDateTimeString();
        try {
            /** Regions */
            UserRegions::where('user_id', $user->id)->delete();
            $dataRegions = [];
            foreach ($regions as $key => $value) {
                array_push($dataRegions, ["user_id" => $user->id, "province_id" => $value, 'created_at' => $now, 'updated_at' => $now]);
            }
            UserRegions::insert($dataRegions);

            /** Categories */
            UserCategories::where('user_id', $user->id)->delete();
            $dataCategories = [];
            foreach ($categories as $key => $value) {
                array_push($dataCategories, ["user_id" => $user->id, "categories_id" => $value, 'created_at' => $now, 'updated_at' => $now]);
            }
            UserCategories::insert($dataCategories);

            /** Salary */
            $request->user()->salary = $salary;
            $request->user()->initialization = 0;
            $request->user()->save();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 500,
                'messages' => 'Failed',
            ], 500);
        }
        return response()->json([
            'status' => 200,
            'messages' => 'Success',
        ], 200);
    }

    public function setResume(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:pdf|max:1024',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $user  = $request->user();

        if ($file = $request->file('file')) {
            $path = $file->storeAs('public/resumes', $user->email . "." . $file->getClientOriginalExtension());
            $user->resume = $user->email . "." . $file->getClientOriginalExtension();
            $user->save();
            return response()->json([
                "success" => true,
                "message" => "File successfully uploaded",
                "file" => $file
            ]);
        }
    }

    public function getResume(Request $request)
    {
        $user  = $request->user();
        return response()->file('app/resumes/' . $user->email . ".pdf", ['Content-Type' => 'application/pdf', 'Content-Disposition' => 'attachment', 'filename' => '$user->email.".pdf"']);
    }
}
