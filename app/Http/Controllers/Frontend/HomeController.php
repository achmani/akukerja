<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Job\Jobs;
use App\Models\Categories;
use App\Models\Location\Province;
use App\Models\Location\Regency;

use Illuminate\Support\Carbon;

use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;

use Stevebauman\Location\Facades\Location;


class HomeController extends Controller
{
    public function index(Request $request)
    {

        $position = Location::get();
        if($position){
            $cityName = $position->cityName;
            // $provinceList = Regency::where('name','LIKE',"%".$cityName."%")->groupBy("province_id")->pluck("province_id")->toArray();
            // $cityList = Regency::whereIn('province_id',$provinceList)->pluck("id")->toArray();
            $cityList = Regency::where('name','LIKE',"%".$cityName."%")->pluck("id")->toArray();
        }

        $description = "Vendor Penyedia Kerja";
        $title = "Home";

        $categories = Categories::get();
        $job_favorite = Jobs::with('companies')->with('regions');
        if($cityList){
            $job_favorite->whereHas('regions', function ($query) use ($cityList) {
                $query->whereIn('region_id', $cityList);
            });            
        }
        $job_favorite = $job_favorite->orderBy('job_posted','DESC')->take(10)->get();
        $regions = Province::get();

        foreach ($job_favorite as $key => $value) {
            if (count($value->regions) > 1) {
                $value->regions = "Multiple Location";
            } else {
                $value->regions = $value->regions[0]->name;
            }
        }

        $seo_jobs = array();
        $seo_jobs['@context'] = "https://schema.org/";

        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
        SEOMeta::addMeta('data:published_time', "2020-12-12", 'property');
        SEOMeta::addMeta('data:section', "music", 'property');
        // SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($request->url());
        OpenGraph::addProperty('type', 'data');
        OpenGraph::addProperty('locale', 'id-id');

        OpenGraph::addImage(asset('image/icon-min.png'));

        JsonLd::setTitle($title);
        JsonLd::setDescription($description);
        JsonLd::setType('Article');
        JsonLd::addImage(asset('image/icon-min.png'));

        return view('frontend/index')
            ->with('regions', $regions)
            ->with('job_favorite', $job_favorite)
            ->with('categories', $categories);
    }

    public function resetPassword(Request $request){
        
        $encryption = base64_decode($request->encrypt);

        // Store cipher method 
        $ciphering = "BF-CBC"; 

        // Use OpenSSl encryption method 
        $iv_length = openssl_cipher_iv_length($ciphering); 
        $options = 0; 

        // Decryption of string process starts 
        // Used random_bytes() which gives randomly 
        // 16 digit values 
        $decryption_iv = random_bytes($iv_length); 

        // Use random_bytes() function which gives 
        // randomly 16 digit values 
        //$encryption_iv = random_bytes($iv_length); 
        $encryption_iv = "�/$%θ";
        
        // Store the decryption key 
        $decryption_key = openssl_digest(php_uname(), 'SHA1', TRUE); 
        
        // Descrypt the string 
        $decryption = openssl_decrypt ($encryption, $ciphering, 
                    $decryption_key, $options, $encryption_iv);
        
        $decryption = explode("|",$decryption);

        if( count($decryption) == 5  ){

            $now = Carbon::now()->timestamp;

            if( $now - $decryption[4] > 3600 ){
                //Expired
                abort(404);
            }

            $user = User::where("email",$decryption[0])->where("ProviderId",$decryption[1])->where("name",$decryption[2])->where("back_name",$decryption[3])->first();
            if($user){
                return view('auth/passwords/reset')
                    ->with("user",$user);
            }else{
                //User not found
                abort(404);
            }
        }else{
            //Decrypt Failed
            abort(404);
        }

        return $decryption;
    }

    public function passwordUpdate(Request $request){
        
        $validatedData = $request->validate([
            'email' => 'email|required|exists:users',
            'password' => 'required|confirmed'
        ]);

        $user = User::where("email",$request->email)->first();
        $user->password = bcrypt($request->password);
        $user->save();

        return back()->with('success','Password Change Successfully');

    }

    public function privacyPolicy(Request $request){
        return view('frontend/privacy');
    }

}
