<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use App\Models\Job\Jobs;
use App\Models\Categories;
use App\Models\Regions;

use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;


class JobController extends Controller
{
    public function index(Request $request)
    {

        $id = $request->id;
        $job = Jobs::with('companies')->with('regions')->findOrFail($id);
        $regencies = $job->regions->pluck('id')->toArray();
        $applicantCategories = $job->categories->pluck('categories_id')->toArray();
        $similar_jobs = Jobs::with(['companies', 'categories', 'regions']);
        if (!empty($regencies)) {
            $data = $similar_jobs->whereHas('regions', function ($query) use ($regencies) {
                $query->whereIn('region_id', $regencies);
            });
        } elseif (!empty($applicantCategories)) {
            $data = $similar_jobs->whereHas('categories', function ($query) use ($applicantCategories) {
                $query->whereIn('jobs_categories.categories_id', $applicantCategories);
            });
        }
        $similar_jobs = $similar_jobs->where('job_id', '!=', $id)->limit(5)->get();
        foreach ($similar_jobs as $key => $value) {
            if (count($value->regions) > 1) {
                $value->regions = "Multiple Location";
            } else {
                $value->regions = $value->regions[0]->name;
            }
        }

        $description = $job->job_description;
        $title = $job->job_title . " - " . $job->companies->company_name;

        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
        SEOMeta::addMeta('data:published_time', $job->job_posted, 'property');
        SEOMeta::addMeta('data:section', "vacancy", 'property');
        // SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($request->url());
        OpenGraph::addProperty('type', 'data');
        OpenGraph::addProperty('locale', 'id-id');
        OpenGraph::addImage(asset('image/icon-min.png'));

        JsonLd::setTitle($title);
        JsonLd::setType('jobPosting');
        JsonLd::setDescription($description);
        JsonLd::addValue('title', $title );
        JsonLd::addValue('identifier', [ "@type" => "PropertyValue", "name" => "Google", "value" => $id ] );
        JsonLd::addValue('datePosted', $job->job_posted );
        JsonLd::addValue('validThrough', $job->job_expired );
        JsonLd::addValue('employmentType', "FULL TIME" );
        JsonLd::addValue('hiringOrganization',  [ "@type" => "Organization", "name" => $job->companies->company_name, "sameAs" => asset($job->companies->company_image), "logo" => asset($job->companies->company_image) ]);
        JsonLd::addValue('jobLocation', [ "@type" => "Place", "address" => [ "@type" => "PostalAddress", "streetAddress" => $job->companies->company_address, "addressLocality" => $job->regions[0]->name, "postalCode" => "", "addressCountry" => "ID" ] ] );
        JsonLd::addValue('baseSalary', [ "@type" => "MonetaryAmount", "currency" => "IDR", "value" => [ "@type" => "QuantitativeValue", "value" =>  $job->min_salary, "unitText" => "MONTH" ] ]);
        

        return view('frontend/job')
            ->with('similar_jobs', $similar_jobs)
            ->with('job', $job);
    }
}
