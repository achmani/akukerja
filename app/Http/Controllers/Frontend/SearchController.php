<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use App\Models\Job\Jobs;
use App\Models\Location\Regency;
use App\Models\Job\JobsRegions;
use App\Models\Job\JobsCategories;
use App\Models\Location\Province;
use App\Models\Categories;
use App\Models\Regions;
use Illuminate\Database\Eloquent\Builder;

/** Javascript */

use Laracasts\Utilities\JavaScript\JavaScriptFacade;

use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;


class SearchController extends Controller
{
    public function index(Request $request)
    {
        $categoriesurl = explode(',', $request->categories);
        $regionsurl = explode(',', $request->regions);
        $keywordurl = $request->keyword;

        $applicantCategories = ($request->categories) ? explode(',', $request->categories) : [];
        $regencies = ($request->regions) ? Regency::whereIn('id', explode(',', $request->regions))->pluck('id')->toArray() : [];
        $regenciesSelect = Regency::whereIn('id', explode(',', $request->regions))->get(['id as id', 'name as text'])->toArray();
        $categories = Categories::get();
        $regions = Province::get();

        $description = "Vendor Penyedia Kerja";
        $title = "Home";

        $data = Jobs::search($keywordurl)->with(['companies', 'categories', 'regions'])
            ->whereHas('regions', function ($query) use ($regencies) {
                if($regencies){
                    $query->whereIn('jobs_regions.region_id', $regencies);
                }
            })->whereHas('categories', function ($query) use ($applicantCategories) {
                if($applicantCategories){
                    $query->whereIn('jobs_categories.categories_id', $applicantCategories);
                }
            });

        $data = $data->paginate(60);
        foreach ($data as $key => $value) {
            if (count($value->regions) > 1) {
                $value->regions = "Multiple Location";
            } else {
                if (count($value->regions) >= 1) {
                    $value->regions = $value->regions[0]->name;
                }
            }
        }

        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
        SEOMeta::addMeta('data:published_time', "2020-12-12", 'property');
        SEOMeta::addMeta('data:section', "music", 'property');
        // SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($request->url());
        OpenGraph::addProperty('type', 'data');
        OpenGraph::addProperty('locale', 'id-id');

        OpenGraph::addImage(asset('image/icon-min.png'));

        JsonLd::setTitle($title);
        JsonLd::setDescription($description);
        JsonLd::setType('Article');
        JsonLd::addImage(asset('image/icon-min.png'));

        JavaScriptFacade::put([
            'keywordurl' => $keywordurl,
            'regionsurl' => $regenciesSelect,
            'categoriesurl' => $categoriesurl,
        ]);

        return view('frontend/search')
            ->with('jobsearch', $data)
            ->with('categoriesurl', $categoriesurl)
            ->with('keywordurl', $keywordurl)
            ->with('regionsurl', $regionsurl)
            ->with('regions', $regions)
            ->with('job_favorite', $data)
            ->with('categories', $categories);
    }
}
