<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stevebauman\Location\Facades\Location;

class GeoLocationController extends Controller
{
    
    public function index(Request $request)
    {
        if ($position = Location::get()) {
            // Successfully retrieved position.
            dd($position);
            echo $position->countryName;
        } else {
            // Failed retrieving position.
        }
    }
    
}
