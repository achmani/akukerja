<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

/** Carbon */

use Illuminate\Support\Carbon;

/** Models */

use App\Models\Job\Jobs;
use App\Models\Categories;
use App\Models\Companies;
use App\Models\Job\JobsCategories;
use App\Models\Job\JobsSpecialization;
use App\Models\Job\JobsRegions;
use App\Models\Job\JobsEducations;
use App\Models\Job\JobsIndustries;
use App\Models\Applicant\Apply;
use App\Models\Regions;
use App\Models\Location\Regency;
use App\Models\Master\EducationMaster;
use App\Models\Master\IndustriesMaster;
use App\Models\Master\SpecializationSubMaster;

/** Auth */

use Illuminate\Support\Facades\Auth;

/** Javascript */

use Laracasts\Utilities\JavaScript\JavaScriptFacade;

/** Meta Tools */

use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\Storage;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:company')->except('logout');
    }

    public function index(Request $request)
    {
        $title = "Dashboard";
        SEOMeta::setTitle($title);
        JsonLd::setTitle($title);

        $job = Jobs::with(['applied'])->paginate(3);
        $now = Carbon::now()->format("Y-m-d");
        //dd($job[0]->applied->where("apply_status",0)->count());
        return view('backend/index')
            ->with('now', $now)
            ->with('job', $job);
    }

    public function vacanciesForm(Request $request)
    {
        $job = null;

        $form = true; // Create

        if ($request->id) {
            $job = Jobs::with(['specializations', 'industries', 'categories', 'regions'])->findOrFail($request->id);
            $form = false; // Update
        }

        $locations = Regency::get();
        $categories = Categories::get();
        $specialization = SpecializationSubMaster::get();
        $industries = IndustriesMaster::get();
        $educations = EducationMaster::get();

        // dd($job->industries()->get(['industries_id as id', 'industries_name as text'])->toArray());
        // dd($job->educations()->get(['educations.education_id as id', 'educations.education_name as text'])->toArray());
        if ($request->id) {
            JavaScriptFacade::put([
                'specialization_sub_id' => $job->specializations()->get(['specialization_sub_id as id', 'specialization_sub_name as text'])->toArray(),
                'industries_id' => $job->industries()->get(['industries.industries_id as id', 'industries.industries_name as text'])->toArray(),
                'education_id' => $job->educations()->get(['educations.education_id as id', 'educations.education_name as text'])->toArray(),
                'categories_id' => $job->categories()->get(['categories.categories_id as id', 'categories.categories_name as text'])->toArray(),
                'region_id' => $job->regions()->get(['id as id', 'name as text'])->toArray(),
            ]);
        }

        return view('backend/vacancies')
            ->with('job', $job)
            ->with('form', $form)
            ->with('locations', $locations)
            ->with('categories', $categories)
            ->with('educations', $educations)
            ->with('specialization', $specialization)
            ->with('industries', $industries);
    }

    public function companiesForm(Request $request)
    {
        $job = null;
        $form = false; // Update
        $company = Companies::findOrFail($request->user()->company_id);
        $industries = IndustriesMaster::get();
        $dataindustries = IndustriesMaster::findOrFail($company->company_industry);
        $arrindustries = [['id' => $dataindustries->industries_id, "text" => $dataindustries->industries_name]];
        // dd($arrindustries);
        JavaScriptFacade::put([
            'industries_id' => $arrindustries,
        ]);

        return view('backend/companies')
            ->with('job', $job)
            ->with('form', $form)
            ->with('company', $company)
            ->with('industries', $industries);
    }

    public function companies(Request $request)
    {
        // dd($request->company_image);
        // $validatedData = $request->validate([
        //     'company_image' => ['max:128'],
        // ]);

        // dd($validatedData);

        try {
            DB::beginTransaction();
            $now = Carbon::now()->format("Y-m-d");
            $expired = Carbon::now()->addWeeks(2)->format("Y-m-d");
            $companies = companies::findOrFail($request->user()->company_id);
            $img_title = md5(microtime());
            // Storage::disk('public')->put("images/companies/" . $img_title . '.jpg', $request->file('company_image'), 'public');
            if ($request->file('company_image')) {
                $request->file('company_image')->storeAs('public/images/companies/', $img_title . ".jpg");
            }
            $companies->company_name = $request->company_name;
            $companies->company_address = $request->company_address;
            $companies->company_mail = $request->company_mail;
            $companies->company_phone = $request->company_phone;
            $companies->company_industry = $request->company_industry;
            $companies->company_overview = $request->company_overview;
            if ($request->file('company_image')) {
                $companies->company_image = "app/images/companies/" . $img_title . ".jpg";
            }
            $companies->company_min_employee = $request->company_employee;
            $companies->company_max_employee = $request->company_employee;
            $companies->save();
            DB::commit();
            return redirect("dashboard/companies")->with('message', 'Data Berhasil diupdate');
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
            Log::debug($e);
        }
    }

    public function applicantUpdate(Request $request)
    {
        $apply = Apply::where("user_id", $request->user_id)->where("job_id", $request->job_id)->update(['apply_status' => $request->apply_status]);
        return json_encode(($apply));
    }

    public function updateVacancies(Request $request)
    {
        $locations = Regency::get();
        $categories = Categories::get();
        $specialization = SpecializationSubMaster::get();
        $industries = IndustriesMaster::get();
        return view('backend/vacancies')
            ->with('locations', $locations)
            ->with('categories', $categories)
            ->with('specialization', $specialization)
            ->with('industries', $industries);
    }

    public function vacancies(Request $request)
    {
        $user = $request->user();
        try {
            DB::beginTransaction();
            $now = Carbon::now()->format("Y-m-d");
            $expired = Carbon::now()->addWeeks(2)->format("Y-m-d");
            if ($request->job_id) {
                $job = Jobs::findOrFail($request->job_id);
            } else {
                $job = new Jobs();
            }
            $job->job_title = $request->job_title;
            $job->company_id = $user->company_id;
            $job->job_posted = $now;
            $job->job_expired = $expired;
            $job->job_description = $request->job_description;
            $job->job_employment_type = $request->job_employment_type;
            $job->job_status = 1;
            $job->min_salary = $request->min_salary;
            $job->max_salary = $request->max_salary;
            $job->save();

            JobsCategories::where('job_id', $job->job_id)->delete();
            $sequence = 0;
            foreach ($request->job_categories as $key => $value) {
                $categories = new JobsCategories();
                $categories->job_id = $job->job_id;
                $categories->job_categories_seq = $sequence++;
                $categories->categories_id = $value;
                $categories->save();
            }

            JobsEducations::where('job_id', $job->job_id)->delete();
            $sequence = 0;
            foreach ($request->job_educations as $key => $value) {
                $education = new JobsEducations();
                $education->job_id = $job->job_id;
                $education->job_educations_seq = $sequence++;
                $education->education_id = $value;
                $education->save();
            }

            JobsIndustries::where('job_id', $job->job_id)->delete();
            $sequence = 0;
            foreach ($request->job_industries as $key => $value) {
                $industries = new JobsIndustries();
                $industries->job_id = $job->job_id;
                $industries->job_industries_seq = $sequence++;
                $industries->industries_id = $value;
                $industries->save();
            }

            JobsSpecialization::where('job_id', $job->job_id)->delete();
            $sequence = 0;
            foreach ($request->job_specialization as $key => $value) {
                $specialization = new JobsSpecialization();
                $specialization->job_id = $job->job_id;
                $specialization->job_specialization_seq = $sequence++;
                $specialization->specialization_sub_id = $value;
                $specialization->save();
            }

            JobsRegions::where('job_id', $job->job_id)->delete();
            foreach ($request->job_locations as $key => $value) {
                $region = new JobsRegions();
                $region->job_id = $job->job_id;
                $region->region_id = $value;
                $region->save();
            }
            DB::commit();
            return redirect("dashboard")->with('message', 'Lowongan berhasil dibuat');
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
            Log::debug($e);
        }
    }

    public function applicant(Request $request)
    {
        $job = Jobs::findOrFail($request->id);
        $user_apply = Apply::where('job_id', $request->id)->where("apply_status", 0)->orWhere("apply_status", 1)->with(['users'])->paginate(10);
        $regions = Regions::get();
        $categories = Categories::get();

        $title = "Applicant - " . $job->job_title;
        SEOMeta::setTitle($title);
        JsonLd::setTitle($title);

        return view('backend/applicant')
            ->with('categories', $categories)
            ->with('regions', $regions)
            ->with('job', $job)
            ->with('user_apply', $user_apply);
    }

    public function applicantSelected(Request $request)
    {
        $job = Jobs::findOrFail($request->id);
        $user_apply = Apply::where('job_id', $request->id)->where("apply_status", 2)->with(['users'])->paginate(10);
        $regions = Regions::get();
        $categories = Categories::get();

        $title = "Applicant Selected - " . $job->job_title;
        SEOMeta::setTitle($title);
        JsonLd::setTitle($title);

        return view('backend/applicant')
            ->with('categories', $categories)
            ->with('regions', $regions)
            ->with('job', $job)
            ->with('user_apply', $user_apply);
    }

    public function applicantInterviewed(Request $request)
    {
        $job = Jobs::findOrFail($request->id);
        $user_apply = Apply::where('job_id', $request->id)->where("apply_status", 3)->with(['users'])->paginate(10);
        $regions = Regions::get();
        $categories = Categories::get();

        $title = "Applicant Interviewed - " . $job->job_title;
        SEOMeta::setTitle($title);
        JsonLd::setTitle($title);

        return view('backend/applicant')
            ->with('categories', $categories)
            ->with('regions', $regions)
            ->with('job', $job)
            ->with('user_apply', $user_apply);
    }

    public function applicantNotsuitable(Request $request)
    {
        $job = Jobs::findOrFail($request->id);
        $user_apply = Apply::where('job_id', $request->id)->where("apply_status", 4)->with(['users'])->paginate(10);
        $regions = Regions::get();
        $categories = Categories::get();

        $title = "Applicant Not Suitable - " . $job->job_title;
        SEOMeta::setTitle($title);
        JsonLd::setTitle($title);

        return view('backend/applicant')
            ->with('categories', $categories)
            ->with('regions', $regions)
            ->with('job', $job)
            ->with('user_apply', $user_apply);
    }
}
