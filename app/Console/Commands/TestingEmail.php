<?php

namespace App\Console\Commands;

use App\Jobs\SendEmail;

use App\Mail\AuthenticationEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Console\Command;

class TestingEmail extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //Mail::to("helmiagilachmani098@gmail.com")->send(new AuthenticationEmail());
        SendEmail::dispatch("helmiagilachmani098@gmail.com","bejo","sasasas");
    }
}
