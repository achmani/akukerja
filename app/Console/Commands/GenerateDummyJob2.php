<?php

namespace App\Console\Commands;

use Faker\Factory;
use Carbon\Carbon;

use App\Models\Job\Jobs;
use App\Models\Categories;
use App\Models\Location\Regency;
use App\Models\Job\JobsDescriptions;
use App\Models\Job\JobsQualifications;
use App\Models\Job\JobsResponsibilities;
use Illuminate\Console\Command;

class GenerateDummyJob2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:job2 {qty}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Get all the categories
        $categories = Categories::all();
        $regions = Regency::all();

        $qty = $this->argument('qty');

        // Populate users
        $jobs = Jobs::factory()->count($qty)->create();
        // $jobs = Jobs::factory()->count(1)->create()->each(function ($job) {
        //     $job->descriptions()->saveMany(JobsDescriptions::class, 5)->make();
        // });
        // Populate the pivot table
        // Jobs::all()->each(function ($job) use ($categories) {
        $jobs->each(function ($job) use ($categories, $regions) {
            $job_categories = $categories->random(rand(1, 3))->pluck('categories_id')->toArray();
            $seq = 1;
            foreach ($job_categories as $key => $value) {
                $current_date_time = Carbon::now()->toDateTimeString(); // Produces something like "2019-03-11 12:25:00"
                $temp = Categories::find($value);
                $job->categories()->attach($temp, ['job_categories_seq' => $seq++, 'created_at' => $current_date_time, 'updated_at' => $current_date_time]);
            }
            
            $job_region = $regions->random(rand(1, 3))->pluck('id')->toArray();
            foreach ($job_region as $key => $value) {
                $current_date_time = Carbon::now()->toDateTimeString(); // Produces something like "2019-03-11 12:25:00"
                $temp = Regency::find($value);
                $job->regions()->attach($temp, ['created_at' => $current_date_time, 'job_id' => $job->job_id, 'updated_at' => $current_date_time]);
            }

        });
    }
}
