<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Shortcut to pluck a field in a relationship.
         *
         * @param string $relationship The relationship name
         * @param string|array $value
         * @param string|null $key
         */
        collect()->macro('pluckDistant', function ($relationship, $value, $key = null) {
            return $this->map(function ($item) use ($relationship, $value, $key) {
                $relation = $item->getRelation($relationship);

                if (
                    get_class($relation) == \Illuminate\Support\Collection::class ||
                    get_class($relation) == \Illuminate\Database\Eloquent\Collection::class
                ) {
                    $item->setRelation($relationship, $relation->pluck($value, $key));
                }

                return $item;
            });
        });
    }
}
