<?php

namespace App\Models\Applicant;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompaniesExperience extends Model
{
    use HasFactory;

    protected $table = 'companies_experience';

    protected $primaryKey = 'company_experience_id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_experience_id',
        'company_experience_name',
    ];
}
