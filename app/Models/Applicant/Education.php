<?php

namespace App\Models\Applicant;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $table = 'users_education';

    protected $primaryKey = 'user_education_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_education_id',
        'user_id',
        'education_id',
        'major_id',
        'institution',
        'user_education_year',
        'user_education_month',
        'user_education_value',
        'user_education_desc'
    ];

    public function major()
    {
        return $this->hasOne('App\Models\Applicant\Major','major_id','major_id');
    }

    public function education()
    {
        return $this->hasOne('App\Models\Master\EducationMaster','education_id','education_id');
    }

}
