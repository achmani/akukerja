<?php

namespace App\Models\Applicant;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expertise extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $table = 'users_expertise';

    protected $primaryKey = ['user_id','user_expertise_seq'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'user_expertise_seq',
        'user_expertise_name',
        'user_expertise_level',
    ];
}
