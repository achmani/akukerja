<?php

namespace App\Models\Applicant;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Apply extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $table = 'users_apply';

    protected $primaryKey = ['user_id', 'job_id'];

    // Carbon instance fields
    protected $dates = ['created_at', 'updated_at', 'job_applied'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'job_id',
        'apply_status',
        'additional_info'
    ];

    public function users()
    {
        return $this->hasOne(
            'App\Models\User',
            'id',
            'user_id'
        );
    }

    public function experience()
    {
        return $this->hasMany(
            'App\Models\Applicant\Experience',
            'user_experience_user_id',
            'user_id'
        );
    }

    public function expertise()
    {
        return $this->hasMany(
            'App\Models\Applicant\Expertise',
            'user_id',
            'user_id'
        );
    }

    public function education()
    {
        return $this->hasMany(
            'App\Models\Applicant\Education',
            'user_id',
            'user_id'
        );
    }

    public function languages()
    {
        return $this->hasMany('App\Models\Applicant\Language', 'user_id', 'user_id');
    }

    // public function company()
    // {
    //     return $this->hasOneThrough(
    //         \App\Models\Applicant\Experience::class,
    //         \App\Models\Applicant\CompaniesExperience::class,
    //         'user_experience_user_id', // Foreign key on the cars table...
    //         'user_id', // Foreign key on the owners table...
    //         'user_experience_company', // Local key on the mechanics table...
    //         'company_experience_id' // Local key on the cars table...
    //     );
    // }

    // public function company()
    // {
    //     return $this->belongsToOne(
    //         'App\Models\Applicant\CompaniesExperience',
    //         'App\Models\Applicant\Experience',
    //         'user_experience_company_id',
    //         'company_id',
    //         'job_id'
    //     );
    // }

}
