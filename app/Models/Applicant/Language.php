<?php

namespace App\Models\Applicant;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $table = 'users_language';

    protected $primaryKey = ['user_id','user_language_seq'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'user_language_seq',
        'user_language',
        'user_language_write',
        'user_language_spoke',
        'user_language_is_first',  
    ];
}
