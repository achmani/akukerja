<?php

namespace App\Models\Applicant;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    use HasFactory;

    public $incrementing = true;

    protected $table = 'users_experience';

    protected $primaryKey = 'user_experience_id';

    //protected $dates = ['created_at', 'updated_at', 'month_start', 'user_experience_end'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_experience_id',
        'user_experience_user_id',
        'user_experience_title',
        'user_experience_position',
        'user_experience_company',
        'user_experience_specialization',
        'user_experience_industry',
        'user_experience_status',
        'user_experience_start',
        'user_experience_end',
        'user_experience_desc',
    ];

    public function company()
    {
        return $this->hasOne('App\Models\Applicant\CompaniesExperience', 'company_experience_id', 'user_experience_company');
    }

    public function specialization()
    {
        return $this->hasOne('App\Models\Master\SpecializationSubMaster', 'specialization_sub_id', 'user_experience_specialization');
    }

    public function industries()
    {
        return $this->hasOne('App\Models\Master\IndustriesMaster', 'industries_id', 'user_experience_industry');
    }

    public function position()
    {
        return $this->hasOne('App\Models\Master\Position', 'position_level_id', 'user_experience_position');
    }
    
}
