<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    use HasFactory;

    protected $table = 'companies';

    public $incrementing = true;

    protected $primaryKey = 'company_id';

    protected $hidden = ['company_status'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'company_name',
        'company_address',
        'company_mail',
        'company_phone',
        'company_industry',
        'company_processing_time',
        'company_benefits',
        'company_min_employee',
        'company_max_employee',
        'company_overview',
        'company_image',
        'company_thumbnail',
        'company_status'

    ];
}
