<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecializationMaster extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $table = 'specializations';

    protected $primaryKey = 'specialization_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'specialization_id',
        'specialization_name',
        'specialization_active'
    ];
}
