<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EducationMaster extends Model
{
    use HasFactory;

    public $incrementing = true;

    protected $table = 'educations';

    protected $primaryKey = 'education_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'education_id',
        'education_name',
        'education_active'
    ];
}
