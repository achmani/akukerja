<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndustriesMaster extends Model
{
    use HasFactory;

    public $incrementing = true;

    protected $table = 'industries';

    protected $primaryKey = 'industries_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'industries_id',
        'industries_name',
        'industries_active'
    ];
}
