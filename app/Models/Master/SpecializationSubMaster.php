<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecializationSubMaster extends Model
{
    use HasFactory;

    public $incrementing = true;

    protected $table = 'specializations_sub';

    protected $primaryKey = 'specialization_sub_id';

    protected $hidden = array('pivot');

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'specialization_sub_id',
        'specializations_id',
        'specialization_sub_name',
        'specialization_sub_active'
    ];
}
