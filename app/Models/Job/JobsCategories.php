<?php

namespace App\Models\Job;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobsCategories extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $table = 'jobs_categories';

    protected $primaryKey = ['job_id', 'job_categories_seq'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id',
        'job_categories_seq',
        'categories_id'
    ];
}
