<?php

namespace App\Models\Job;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobsResponsibilities extends Model
{
    use HasFactory;

    public $incrementing = false;
    protected $table = 'jobs_responsibilities';
    protected $primaryKey = ['job_id', 'job_responsibilities_seq'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id',
        'job_responsibilities_seq',
        'job_responsibilities_text'
    ];
}
