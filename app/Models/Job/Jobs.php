<?php

namespace App\Models\Job;

use App\Scopes\JobScope;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Searchable;

class Jobs extends Model
{
    use HasFactory;
    use Searchable;

    public $searchable = ['job_title', 'job_description', 'job_posted', 'job_expired'];

    protected $table = 'jobs';

    protected $primaryKey = 'job_id';

    protected $hidden = ['pivot'];

    // Carbon instance fields
    protected $dates = ['created_at', 'updated_at', 'job_posted', 'job_applied'];

    protected static function booted()
    {
        static::addGlobalScope(new JobScope);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id',
        'job_title',
        'job_posted',
        'job_description',
        'job_posted',
        'min_salary',
        'max_salary',
        'job_expired'
    ];

    public function categories()
    {
        return $this->belongsToMany(
            'App\Models\Categories',
            'App\Models\Job\JobsCategories',
            'job_id',
            'categories_id'
        );
    }

    public function jobsCategories()
    {
        return $this->belongsToMany(
            'App\Models\Categories',
            'App\Models\Job\JobsCategories',
            'job_id',
            'categories_id'
        );
    }

    public function bookmarked()
    {
        return $this->hasMany(
            'App\Models\Applicant\Bookmark',
            'job_id',
            'job_id'
        );
    }

    public function applied()
    {
        return $this->hasMany(
            'App\Models\Applicant\Apply',
            'job_id',
            'job_id'
        );
    }

    public function companies()
    {
        return $this->hasOne('App\Models\Companies','company_id','company_id');
    }

    public function specializations()
    {
        return $this->belongsToMany(
            'App\Models\Master\SpecializationSubMaster',
            'App\Models\Job\Jobs',
            'job_id',
            'job_id'
        );
    }

    public function educations()
    {
        return $this->belongsToMany(
            'App\Models\Master\EducationMaster',
            'App\Models\Job\JobsEducations',
            'job_id',
            'education_id'
        );
    }

    public function industries()
    {
        return $this->belongsToMany(
            'App\Models\Master\IndustriesMaster',
            'App\Models\Job\JobsIndustries',
            'job_id',
            'industries_id'
        );
    }

    public function regions()
    {
        return $this->belongsToMany(
            'App\Models\Location\Regency',
            'App\Models\Job\JobsRegions',
            'job_id',
            'region_id'
        );
    }
    

    public  function scopeRegionsSearch($query, $value){
            return $query->join('jobs_regions', 'jobs.job_id', 'jobs_regions.job_id')
                ->whereIn('jobs_regions.region_id', $value);
    }

    public  function scopeCategoriesSearch($query, $value){
        return $query->join('jobs_categories', 'jobs.job_id', 'jobs_categories.job_id')
            ->whereIn('jobs_categories.categories_id', $value);
}

    // public function descriptions()
    // {
    //     return $this->hasMany('App\Models\Job\JobsDescriptions','job_id','job_id');
    // }

    // public function qualifications()
    // {
    //     return $this->hasMany('App\Models\Job\JobsQualifications','job_id','job_id');
    // }

    
    // public function responsibilities()
    // {
    //     return $this->hasMany('App\Models\Job\JobsResponsibilities','job_id','job_id');
    // }


}
