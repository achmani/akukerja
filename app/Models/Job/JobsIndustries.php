<?php

namespace App\Models\Job;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobsIndustries extends Model
{
    use HasFactory;

    public $incrementing = false;
    protected $table = 'jobs_industries';
    protected $primaryKey = ['job_id', 'job_industries_seq'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id',
        'job_industries_seq',
        'industries_id'
    ];
}
