<?php

namespace App\Models\Job;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobsSpecialization extends Model
{
    use HasFactory;

    public $incrementing = false;
    protected $table = 'jobs_specializations';
    protected $primaryKey = ['job_id', 'specialization_sub_id','job_specialization_seq'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id',
        'job_specialization_seq',
        'specialization_sub_id'
    ];
}
