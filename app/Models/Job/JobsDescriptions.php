<?php

namespace App\Models\Job;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobsDescriptions extends Model
{
    use HasFactory;

    public $incrementing = false;
    protected $table = 'jobs_descriptions';
    protected $primaryKey = ['job_id', 'job_descriptions_seq'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id',
        'job_descriptions_seq',
        'job_descriptions_text'
    ];
}
