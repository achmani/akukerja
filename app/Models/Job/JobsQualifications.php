<?php

namespace App\Models\Job;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobsQualifications extends Model
{
    use HasFactory;

    public $incrementing = false;
    protected $table = 'jobs_qualifications';
    protected $primaryKey = ['job_id', 'job_qualifications_seq'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id',
        'job_qualifications_seq',
        'job_qualifications_text'
    ];
}
