<?php

namespace App\Models\Job;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobsRegions extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $table = 'jobs_regions';

    protected $primaryKey = ['job_id', 'region_id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id',
        'region_id'
    ];
}
