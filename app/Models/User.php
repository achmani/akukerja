<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'back_name',
        'email',
        'phone_number',
        'gender',
        'initialization',
        'birth_date',
        'password',
        'FirebaseUID',
        'PhotoUrl',
        'salary',
        'resume',
        'ProviderId'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'initialization' => \App\Casts\BooleanCast::class,
        'salary' => \App\Casts\SalaryCast::class,
        // 'gender' => \App\Casts\BooleanCast::class,
        'gender_name' => \App\Casts\GenderCast::class,
    ];  
    
}
