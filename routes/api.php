<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\BookmarkController;
use App\Http\Controllers\API\SelectController;
use App\Http\Controllers\API\ApplyController;
use App\Http\Controllers\API\ProfileController;
use App\Http\Controllers\API\JobsController;
use App\Http\Controllers\API\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [AuthController::class, 'register']);

// Route::post('/login', [AuthController::class, 'login'] );
Route::post('/login_firebase', [AuthController::class, 'login_firebase'])->name('login_firebase');
Route::post('/reset_password', [AuthController::class, 'reset_password'])->name('reset_password');
Route::post('/login_email', [AuthController::class, 'login_email'])->name('login_email');

Route::get('/detail/{id}', [JobsController::class, 'detail']);

Route::get('/lists/{page}/{results}', [JobsController::class, 'list']);
Route::get('/search/{page}/{results}', [JobsController::class, 'search']);

Route::get('/nearest/{longitude}/{latitude}', [JobsController::class, 'getNearestLocation']);


//Select2
Route::get('/select/specialization', [SelectController::class, 'selectSpecialization'])->name("select_specialization");
Route::get('/select/region', [SelectController::class, 'selectRegion'])->name("select_region");
Route::get('/select/industries', [SelectController::class, 'selectIndustries'])->name("select_industries");


// Route::middleware('auth:api')->group(function () {

/** Master Data API */
Route::get('/provinces/list/{keyword?}', [ProfileController::class, 'listProvince']);
Route::get('/provinces/list', [ProfileController::class, 'listProvince']);
Route::get('/regencies/list/{id}/{keyword?}', [ProfileController::class, 'listRegency']);
Route::get('/categories/list/{keyword?}', [ProfileController::class, 'listCategories']);
Route::get('/categories/list', [ProfileController::class, 'listCategories']);
Route::get('/specialization/list/{keyword?}', [ProfileController::class, 'listSpecialization']);
Route::get('/specialization/detail/{id}/{keyword?}', [ProfileController::class, 'detailSpecialization']);
Route::get('/industries/list/{keyword?}', [ProfileController::class, 'listIndustries']);
Route::get('/companies/{list}/{keyword?}', [ProfileController::class, 'listCompanies']);
Route::get('/companies_experience/{list}/{keyword?}', [ProfileController::class, 'listCompaniesExperience']);
Route::get('/position/{list}/{keyword?}', [ProfileController::class, 'listPosition']);
Route::get('/educations/list/{keyword?}', [ProfileController::class, 'listEducations']);
Route::get('/majors/list/{keyword?}', [ProfileController::class, 'listMajors']);
//Route::post('/experience/list/{keyword}', [ProfileController::class, 'createExperience'] );

Route::middleware('auth:api')->group(function () {

    /** User */
    Route::get('/profile', [ProfileController::class, 'getProfile']);
    Route::post('/profile', [ProfileController::class, 'setProfile']);

    /** education */
    Route::post('/education', [ProfileController::class, 'setEducation']);
    Route::get('/education', [ProfileController::class, 'getEducation']);
    Route::delete('/education/{id}', [ProfileController::class, 'deleteEducation']);
    Route::put('/education/{id}', [ProfileController::class, 'setEducation']);

    /** Experience */
    Route::post('/experience', [ProfileController::class, 'setExperience']);
    Route::put('/experience/{id}', [ProfileController::class, 'setExperience']);
    Route::delete('/experience/{id}', [ProfileController::class, 'deleteExperience']);
    Route::get('/experience', [ProfileController::class, 'getExperience']);
    Route::get('/experience/{id}', [ProfileController::class, 'detailExperience']);

    /** Language */
    Route::post('/language', [ProfileController::class, 'setLanguage']);
    Route::get('/language', [ProfileController::class, 'getLanguage']);

    /** Expertise */
    Route::post('/expertise', [ProfileController::class, 'setExpertise']);
    Route::get('/expertise', [ProfileController::class, 'getExpertise']);

    /** Bookmark */
    Route::get('/bookmark', [BookmarkController::class, 'getBookmark']);
    Route::post('/bookmark/{id}', [BookmarkController::class, 'setBookmark']);
    Route::delete('/bookmark/{id}', [BookmarkController::class, 'deleteBookmark']);

    /** Apply */
    Route::get('/apply', [ApplyController::class, 'getApply']);
    Route::post('/apply/{id}', [ApplyController::class, 'setApply']);

    /** Profile */
    Route::post('/user/initialization', [ProfileController::class, 'initialization']);
    Route::post('/resume', [ProfileController::class, 'setResume']);
    Route::get('/resume', [ProfileController::class, 'getResume']);
});
