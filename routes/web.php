<?php
//Controller
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Frontend\SearchController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\JobController;
use App\Http\Controllers\GeoLocationController;
use App\Http\Controllers\AuthCompany\AuthCompanyController;
//API
use App\Http\Controllers\API\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('get-address-from-ip',[GeoLocationController::class, 'index']);

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('/reset_password', [HomeController::class, 'resetPassword'] )->name('password.reset');
Route::get('/privacy_policy', [HomeController::class, 'privacyPolicy'] )->name('privacy.policy');
Route::post('/password_update', [HomeController::class, 'passwordUpdate'] )->name('password.update');

Route::get('/search', [SearchController::class, 'index'])->name('search');
Route::get('/job/{id}', [JobController::class, 'index'])->name('job');
Route::get('/login', [AuthCompanyController::class, 'index'] )->name('login');
Route::post('/login', [AuthCompanyController::class, 'login'])->name('login.auth');
Route::post('/logout', [AuthCompanyController::class, 'logout'])->name('logout.auth');

Route::get('/signup', [AuthCompanyController::class, 'viewSignup'] )->name('signup.view');
Route::post('/signup', [AuthCompanyController::class, 'signup'] )->name('signup.auth');

Route::prefix('dashboard')->as('company.')->group(function() {
    Route::get('/',  [DashboardController::class, 'index'] )->name('home');
    Route::get('/applicant/{id}',  [DashboardController::class, 'applicant'] )->name('applicant');
    Route::get('/applicant/selected/{id}',  [DashboardController::class, 'applicantSelected'] )->name('applicant.selected');
    Route::get('/applicant/interviewed/{id}',  [DashboardController::class, 'applicantInterviewed'] )->name('applicant.interviewed');
    Route::get('/applicant/notsuitable/{id}',  [DashboardController::class, 'applicantNotsuitable'] )->name('applicant.notsuitable');
    Route::get('/vacancies/create',  [DashboardController::class, 'vacanciesForm'] )->name('vacancies.form.create');
    Route::get('/companies',  [DashboardController::class, 'companiesForm'] )->name('companies.form.create');
    Route::post('/companies/update',  [DashboardController::class, 'companies'] )->name('companies.update');
    Route::get('/vacancies/update/{id}',  [DashboardController::class, 'vacanciesForm'] )->name('vacancies.form.update');
    Route::post('/vacancies', [DashboardController::class, 'vacancies'])->name('vacancies.create');
    Route::put('/vacancies', [DashboardController::class, 'vacancies'])->name('vacancies.update');

    Route::post('/applicant/update', [DashboardController::class, 'applicantUpdate'])->name('applicant.update');

 });

Route::get('/firebase_auth', function () {
    return view('firebase_auth');
});

Route::get('/firebase_auth_2', function () {
    return view('firebase_auth_2');
});


//For React Only
// Route::get( '/{path?}', function(){
//     return view('index');
// })->where('path', '.*');Auth::routes();

Route::get('/home', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');

